# Welcome to this super short docker quickstart

Done for Ubuntu like setup ;)


- [Welcome to this super short docker quickstart](#welcome-to-this-super-short-docker-quickstart)
- [install docker](#install-docker)
  - [install it from the official website](#install-it-from-the-official-website)
  - [troubleshooting](#troubleshooting)
    - [sudo required](#sudo-required)
    - [proxy docker](#proxy-docker)
    - [graphical interface](#graphical-interface)
- [short example](#short-example)
  - [from an online image :](#from-an-online-image-)
  - [building an image (python example) :](#building-an-image-python-example-)
- [more compelx example, with ports and volume management](#more-compelx-example-with-ports-and-volume-management)
- [usefull commands](#usefull-commands)



# install docker

## install it from the official website

refer to the [official website](https://docs.docker.com/engine/install/ubuntu/).


## troubleshooting


### sudo required 
```
sudo groupadd docker
sudo groupadd docker
sudo usermod -aG docker $USER
sudo chmod 666 /var/run/docker.sock
```

### proxy docker

Firefox
Manual proxy configuration (exterior)

```
sudo mkdir /etc/systemd/system/docker.service.d
sudo vi docker.service.d/http-proxy.conf
```

```
[Service]
Environment="HTTP_PROXY=http://proxy:3128"
Environment="HTTPS_PROXY=https://proxy:3128"
```

systemctl show --property=Environment docker

### graphical interface

Running an X11 application inside a docker container
https://github.com/Erdwolf/docker-xeyes

```
#Add user to docker group (if not already added)
sudo usermod -aG docker $USER
#add permission X server for docker in host system
xhost +SI:localuser:root
xhost +
```


test with: 
```
docker run -it --rm -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=$DISPLAY fr3nd/xeyes
```

# short example

## from an online image : 
```
sudo systemctl start docker
sudo docker run hello-world
```

@ ISAE think that you are behind a proxy: 

```
sudo docker run hello-world --env HTTP_PROXY="http://proxy.isae.fr:3128"
```

## building an image (python example) : 


https://docs.docker.com/language/python/build-images/

```
git clone https://github.com/docker/python-docker
...

```

# more compelx example, with ports and volume management

check dockerfile of : 
https://gitlab.isae-supaero.fr/jsatorb-dev/luplink/-/tree/master/luplink

and docker-compose of https://gitlab.isae-supaero.fr/jsatorb-dev/luplink


# usefull commands

```
#bash entry in a container: 
#(allow you to "play" interactively with a container you are building)
docker run -ti --entrypoint /bin/bash simu:latest


#list all containers
docker ps -l

#list images
docker images

#remove unused images
docker image prune

# also If -a is specified, will also remove all images not referenced by any container.
docker image prune -a


docker rmi celestrak-json-proxy:dev
docker system prune

sudo systemctl restart docker.service

#cleaning images fail build
docker rmi e9264bc30511 ad3b287bad08

#iterative mode (rm all at the end)
docker run   --rm   -it   centos:centos7 bash

#stop all containers:
docker kill $(docker ps -q)

#remove all containers
docker rm $(docker ps -a -q)

remove all docker images
docker rmi $(docker images -q)


## if forgetting files in docker : 
# we suppose that 5b9bc2ac1611 is the container..
# docker ps to get your container id
sudo docker cp myFile.py 5b9bc2ac1611:/usr/src/app
sudo docker cp myDataFile.csv 5b9bc2ac1611:/usr/src/app

```

