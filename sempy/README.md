
Original doc : [Sempy-dev](https://gitlab.isae-supaero.fr/sempy-dev/sempy/-/blob/master/README.md)

# SEMpy Devs: Guide and Best practices

@author: Emmanuel BLAZQUEZ, Thibault GATEAU

## Introduction

Welcome to SEMpy's user guide. Please read it thoroughly before proceeding with the installationof the library. If you have any questions please check first the links provided in the guide and, if need be, send us an email to [emmanuel.blazquez at isae-supaero.fr] or [thibault.gateau at isae-supaero.fr].

This guide is meant to help developers to get into SEMpy code, sharing framework and development methodology.

## Outline
 1. Why should I use SEMpy?
 2. Getting started
 3. Development framework setting
 4. Developing on SEMpy
 5. Troubleshooting 

## 1. Why should I use SEMpy?

### 1.1 SEMpy is Open source

SEMpy is an open source library (AGPLv3).

### 1.2  SEMpy is dedicated to non-Keplerian environments

SEMpy focuses on non keplerian space mechanics. For Keplerian orbital mechanics in python, please refer to
[Poliastro](https://pypi.org/project/poliastro/), built uppon 
[Astropy](https://www.astropy.org) ,more astronomy oriented.
For other pograming languages, have a look at [GMAT](https://opensource.gsfc.nasa.gov/projects/GMAT/index.php) (C++) or [Orekit](https://www.orekit.org)(Java and its python wrapper).
But for non-Keplerian mechanics, we have chosen python 3 to build a state of the art library.

###  1.3 SEMpy is following standards and includes a development framework

If you respect the established standards, it will ease teamwork, development efficiency and sustainability. The framework presented below aims at easing the development process for the SEMpy development team.


## 2. Getting started

### 2.1 Dependencies for SEMpy developers

#### 2.1.1 Python 3 

SEMpy is based on Python 3. We recommend the use of Anaconda (3.7) distribution, which includes most of the required libraries and tools used for SEMpy.

https://www.anaconda.com/distribution/

#### 2.1.2 Git client

For more information, please refer to the "Git" section of this guide. At this point, just install a git client.

##### Linux - Debian

```bash
sudo apt install git
```

##### Windows

A git client application you can use: [TortoiseGit](https://tortoisegit.org/).
Another option: [SmartGit](https://www.syntevo.com/smartgit/).



### 2.2 Installing  SEMpy's  main core modules 

#### 2.2.1 Linux - Debian

##### Clone the SEMpy git repository in your working folder

We recommend you to have a fixed 'git' working directory: 
```bash
mkdir -p git
cd git
```

```bash
git clone https://gitlab.isae-supaero.fr/sempy-dev/sempy.git
cd sempy
```

##### Install dependencies through pip package manager

```bash
pip install setuptools
```

```bash
python setup.py bdist_wheel
pip install -e ./
```

##### Testing installation
```bash
cd src/tests
python -m unittest
```

If all tests succeeded, SEMpy lib core is now ready to use! 

#### 2.2.2 Windows

As SEMpy is intended to be used in several development environments, an easy method to install
SEMpy was implemented through the use of pip - a package manager for Python


A few steps should be followed in order to achieve the installation of SEMpy. It is assumed that the user has cloned SEMpy's git repository on its local device.

1. In the Anaconda prompt terminal, add the conda-forge channel and install the spiceypy package:
```
conda config --add channels conda-forge
conda install spiceypy
```

2. Go to your local SEMpy repository.

eg:
```
cd C:\Users\r.feynman\sempy
```

Build package information and whl (wheel format) file, and install SEMpy in development mode.

```
python setup.py bdist_wheel
pip install -e ./
```

This will install all main dependencies of the SEMpy package (NumPy, SciPy, etc.) in your environment.

3. Run all unit tests to check if the installation has proceeded smoothly. To do so, go to the tests folder of the package, eg:

```
cd C:\Users\r.feynman\SEMpy\src\tests
```

and execute in the anaconda prompt terminal:

```
python -m unittest
```

If all tests succeeded, SEMpy is now ready to use! 


### 2.3. Installing the low-thrust module (optional)

1. Please follow the dedicated guides to setup a dedicated Python environment [on your local machine](guides\lowthrustInstallation.md) or [on ISAE-Supaero's supercomputer PANDO](lowthrustInstallationPando.md).

2. Follow the [low-thrust module](guides\lowthrustGuide.md) user guide to get started.


## 3. Development framework setting
Listed here is the recommended development framework for SEMpy.
1. IDE: Spyder
2. Code versioning: [Git](git-scm.com)
     - basics
     - branching strategies 
3. coding conventions
     - PEP 8
    - testing your code (Unit testing, integration testing)
    - static code analysis: [Pylint](https://www.pylint.org/)
4. continuous integration: [Jenkins](https://jenkins.io/)
4. Following the development workflow detailed in Section 4.4 .

### 3.1. Installing the framework

 - [Spyder](https://www.spyder-ide.org/) IDE is coming with Anaconda distribution
 - Git: see dedicated section just below
 - static code analysis: 

 ```bash
pip install pylint
pip install coverage
 ```

### 3.2. Code versioning with Git

#### 3.2.1 What is Git?

Git is the most widely used version control system in the world. It is a mature and actively mantained open-source project.
In a nutshell: rather than having a single place to store the full version of a software, by using Git every developper can work on its own copy/version/branch of the code and push it when changes are made.

Additionnally, the repository in which the code is stored contains the full history of all changes on all branches of the development.

Before proceeding any further, please read and refer to https://git-scm.com/docs/gittutorial  to learn how to use Git. For some interactive lessons on how to use git, you can also refer to https://www.katacoda.com/courses/git. Another interactive way to learn more about git branching: https://learngitbranching.js.org/.

### 3.2.2. The SEMpy Git repository

The repository is divided into different branches with different rules for each of them

+ The **master** branch hosts the latest stable, public version of SEMpy. **Only the project managers may commit on te master branch.**
+ The **dev** branch hosts the version under development of SEMpy. The development version must be stable and all changes submitted to it validated by the project managers.
The development branch will be updated following **merge requests** made by the managers to each individual developper.
+ Each developer will work on a **personal sub-branch** of the development branch.

### 3.2.3. Installing Git on Linux 

If not already done, in the linux command line enter:

```bash
sudo apt-get install git
```

or 

```
tortoise - https://tortoisegit.org/
```

Make sure you have installed git correctly (again, visit https://git-scm.com/docs/gittutorial) and have a git repository in your home folder or somwhere easily accessible.



### 3.2.4. Basic Git commands

The first important thing to do once git is installed is to clone the git repository on your machine.

To **clone** the git repository to your local repository, use:

```
cd YourGitFolder
git clone https://openforge.isae.fr/git/sempy
```


To **update** the local repository:

```
git pull origin master
```

To **create** your own local branch and start working on it:

```
git co -b  myLovelyBranchName
```

Get used to shortcuts! e.g, here, 'co' is for "checkout".. This command actually includes implicitely a lot of others command, we'll see them later. Working on your own branch allow you to:
- experiment whatever you need
- not be stressed by polluting/being polluted by the team work
- backup and share it anyway


After every important change made on your version of the code, you will need to commit the new code with a message describing what you have done. This is very iomportant to preserve a clean history of all changes made on the library.
To **commit** your code, use:


```
git commit -am "[myTag] my super message describing what I have done (it's compulsory)" 
```

e.g.
```
git ci -am "[doc] summarizing the 'Theory of Everything'" 
```

After **every day** working on the code, you will need to push your branch of development to the remote server hosting the repository. This will update your branch and add to the repository history all the commits done during the day.
To **push** to the remote repository:

```
git push origin master
```

### 3.2.5. Other useful commands

To **add a file**:

```
git add myFile.txt
```


To change branches from master to myBranch:

```
git co myBranch
```


To switch branches in general:

```
git checkout <branchname>
```


### 3.2.6. Installing Git on Windows

We recomment installing and using Smartgit as a free Git graphical client.

Steps for installation:

1. Install SmartGit. Launch it.
2. Clone the repository by clicking on _Remote Git or SVN repository_, enter "https://openforge.isae.fr/git/SEMpy" in _Repository URL_, slect Git as the type of repository.
3. Login using your ISAE credentials.
4. _Selection, customize how and what to clone_: check all.
5. Enter the local destination address in __Local directory__ and finish the installation.

#### Using SmartGit

The same rules as the one sstated before apply. Save your changes by clicking _Commit_ and add a message explaining what you have done. Push by clicking the dedicated button to save your commits on the online repository.
If you need to go back to an older version of the code (that is to say the state of the code at an older commit), right-click on the commit you wish to go back to (list of commits available in the "Journal" at the bottom of the GUI) and select _Revert & Commit_.





## 4. Developing on SEMpy

### 4.1 Coding conventions

Python Enhancement Proposals 8 (**PEP 8**) is a set of coding conventions for Python code comprising the Python standrad library and its main distributions.
The guide is updated occasionally as new conventions are identified and older rules are rendered obsolete by newer evolutions the language. 

The complete set of conventions can be found on the python website https://www.python.org/dev/peps/pep-0008/. Please read it carefully and apply all these conventions to the code you develop.
**No merge requests with codes that won't comply with the PEP 8 standards will be accepted.**



### 4.2 Testing

Testing is an essential part of any large-scale development process, and ensures the stability of your development branch at any given time. The two main categories of testing you should be aware of as contributors are:

+ **Unit tests** are conducted to test the unity of the code, at module and component level. Individual units of source code are tested to validate their basic functionalities. Unit testing plays an essential role in the detection of bugs at a very early phase of the development.
The idea behind Unit Testing is to test each part of the program and show that the individual parts are correct.
+ **Integration tests** are tests where individual units of a program or library are tested as a group, in combination with one another. Among other things, 	
integration tests verify that your code works with external dependencies correctly.

Unit testing is commonly automated but can still be performed manually. It's a systematic step in any development process and must be ensured at all times. Good practice is to write your unit testing functions as you write your code, and not after.
SEMpy uses the unittest unit testing framework that supports test automation. You can find documentation on the frameowrk at the following address: https://docs.python.org/3/library/unittest.html.

+ **Static tests**
Static analysis is the process of detecting errors and defects in a software or library at source code level. It is an automated code review process. The analysis highlights bugs and weaknesses in the source code. Static analysis is also commonly used to verify that a code complies with certain coding guidelines.

For static testing we are using Pylint. You can learn how to install it and use it in the dedicated section of the testing routines guide.

At this moment we are working on integrating automated static code analysis in SEMpy's development framework. In the meanwhile, we highly encourage every contributor to perform all relevant checks manually.


+ **Tests coverage**
on going...


### 4.3. Documentation

Each piece of code in the SEMpy library needs to be fully documented. _Python Docstring_ refers to the documentation string that occurs in every Python class, module, function or method definition and is written as a first statement.  Docstrings are accessible from the doc attribute for any of the Python object.
They are a descriptive text written by a programme to know what a line of code or expression does. It is an essential part of any development process.

Many Docstrings formats are available. It is however better to use formats that are widely spread and recognized by a large potion of the Pytthon community.
SEMpy uses the **NumPy/SciPy docstrings formatting** type. You can find information on the format in the dedicated website: https://numpydoc.readthedocs.io/en/latest/format.html.
**No merge requests with codes that won't comply with the NumPy/SciPy Docstrings formatting will be accepted.**

The format is supported by Sphinx, the main python documentation generator. Sphinx outputs HTML and LaTeX standardized documentations for any code following the reStructuredText markup language. Provided you have followed the Documentation guidelines stated above, you will be able to generate a Sphinx documentation of your code very easily by following some simple guidelines detailed on the following website: http://www.sphinx-doc.org/en/master/.


#### Installing the Sphinx package

1. Install the Sphinx package, if it is not installed yet.
```
pip install sphinx
```
2. Install other dependencies for documentation generation:

```
conda install -c anaconda sphinx_rtd_theme
conda install -c anaconda graphviz
pip install recommonmark
pip install sphinx-markdown-tables
pip install openmdao
pip install git+https://github.com/OpenMDAO/dymos.git
```

3. In the package folder `docs\source`, edit the file `index.rst` according to: [sphinx documentation](https://www.sphinx-doc.org/en/master/usage/extensions/autodoc.html)


#### Generating your Sphinx documentation
Provided your code follows the NumPy/SciPy docstrings formatting, two simple steps will give you a Sphinx documentation of the library:

1. In the Anaconda prompt go to the docs folder of the library. eg:
```
cd C:\Users\r.feynman\SEMpy\docs
```
2. Compile the documentation:
```
make html
```
The documentation can be reviewed in _docs\build\html\index.html_.



### 4.4 Development Workflow

Featured in this section is the typical development cycle that every developer must respect when contributing to the library and adding new features to SEMpy.

Follow the steps:

1. Write your feature code
2. Write the unit test for this feature
3. Validate your code locally
    - using pylint for static code analysis
    - running all unit tests of the library (see dedicated section)
4. Write integration tests corresponding to your new features
5. Validate all integration tests of the library
6. Check for remote **dev** branch updates, pull them and merge them on your local branch, correct potential conflicts
    ```bash
    git co dev
    git pull origin dev
    git co myLocalBranch
    git merge dev 
    ```
7. Re-validate your code (updated with the on-going dev) 
    - Static code analysis 
    - Unit tests
    - Integration tests
8. Make a merge request
9. Be patient! The admins will review your request and contact you if necessary.
10. Check if the tests are also a success on gitlab.

If one of these steps fails, then you need to stop the workflow, correct the bug, and restart the workflow from the beginning.



## 5. Troubleshooting

### Configuring git behind a proxy

To configure git at work behind a proxy, run this command with your network configuration and credentials :

```bash
git config --global http.proxy http://proxyuser:proxypwd@proxy.server.com:8080
```

### Versions, import errors

```bash
which python
which pip
```


