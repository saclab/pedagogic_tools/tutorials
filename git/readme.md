
## Command line instructions

You can also upload existing files from your computer using the instructions below.
Git global setup

```
git config --global user.name "Perfect Johnny"
git config --global user.email "johnny.perfect@isae.fr"
```

## Create a new repository (johnny-project)

```
git clone git@gitlab.isae-supaero.fr:saclab/dss/students/johnny-project.git
cd default-page
touch README.md
git add README.md
git commit -m "[doc] add README"
git push -u origin master
```

## Push an existing folder

```
cd existing_folder
git init
git remote add origin git@gitlab.isae-supaero.fr:saclab/dss/students/johnny-project.git
git add .
git commit -m "[doc] Initial commit"
git push -u origin master
```

## Push an existing Git repository

```
cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.isae-supaero.fr:saclab/dss/students/johnny-project.git
git push -u origin --all
git push -u origin --tags
```

