#/bin/bash

# run: 
#cd $HOME/SVN/spacescriptsgeneric/scripts/Utils
#sudo bash scriptUpdateGit.bash


#GITPATH="$HOME/git"
GITPATH="$PWD"

OUTPUT_FILE="${GITPATH}/infoGit.txt"
#OUTPUT_FILE=infoGit.txt

#GITREPO="https://gitlab.com/eyesat-ccc/scc-calibrator.git"
#GITREPO="https://gitlab.com/eyesat-ccc/scc-synoptic-handler"
GITREPO="https://gitlab.com/eyesat-ccc/"



REPO=("scc" "scc-build" "scc-procedure" "scc-packet-codec" "scc-app" "scc-parent" "scc-tm-handler" "scc-calibrator" "scc-tc-handler" "scc-xtce-generator" "scc-data-archiver" "scc-tc-verif-service" "scc-xtce-parser" "scc-kernel" "scc-time-correlator" "scc-synoptic-handler" "scc-procedure-spell" "scc-procedure-handler" "scc-procedure-generator" "scc-procedure-generator-test" "scc-procedure-installer" "scc-time-converter")

#MANUAL 
#git clone https://gitlab.com/eyesat-ccc/scc-build.git
#git clone https://gitlab.com/eyesat-ccc/scc-procedure-installer.git

#git clone https://gitlab.com/eyesat-ccc/scc-procedure-spell.git
#git clone https://gitlab.com/eyesat-ccc/scc-procedures/scc-procedure-handler.git
#git clone https://gitlab.com/eyesat-ccc/scc-procedures/scc-procedure-generator.git
#git clone https://gitlab.com/eyesat-ccc/scc-procedures/scc-procedure-generator-test.git
#git clone https://gitlab.com/eyesat-ccc/scc-procedures/scc-procedure.git
#git clone https://gitlab.com/eyesat-ccc/scc-procedures/scc-procedure-installer.git


#scc-xtce-parser"


cd $GITPATH

if [ -f $OUTPUT_FILE ]; then
   mv $OUTPUT_FILE $OUTPUT_FILE.bak
fi


for ((i = 0; i < ${#REPO[@]}; i++))
do
    cd ${GITPATH}/
    echo "Proceeding: ${REPO[$i]}"
    echo "==========" >> $OUTPUT_FILE
    echo "$GITPATH/${REPO[$i]}">> $OUTPUT_FILE
    echo "==========" >> $OUTPUT_FILE
    
    FLAGREPO=$(find . -maxdepth 1 -type d   -name "${REPO[$i]}")
    if [ -z ${FLAGREPO} ]; 
    then
        git clone ${GITREPO}${REPO[$i]}
    else

        #ls ${GITPATH}/${REPO[$i]} >> $OUTPUT_FILE
        cd ${GITPATH}/${REPO[$i]} 

	#git up
        #git st

        git remote update -p
        git merge --ff-only @{u}
        git status


    fi

    echo " " >> $OUTPUT_FILE
    #echo "Press a key..."
    #read a
done

cd $GITPATH





#############################



# run: 
#cd $HOME/SVN/spacescriptsgeneric/scripts/Utils
#bash scriptUpdateGit.bash


#git checkout -b testingTestunit
#git ci -am "adding some stuff in bdd"
#git co master
#git pull


#GITREPO="https://gitlab.com/eyesat-ccc/scc-app"




