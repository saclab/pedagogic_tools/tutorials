
GITPATH=~/git

function gsm {
gnome-system-monitor &
}
alias gsm='gsm'

function gob {
cd ~ &&
cd ${GITPATH}/biblio/ &&
gitup && 
git st
}
alias gob='gob'

function gos {
cd ~ &&
cd ${GITPATH}/spacescriptsgeneric/ &&
gitup
}
alias gos='gos'

function gitup {
git pull --rebase origin master 
}
alias gitup='gitup'

function gitpush {
if git push origin master; then 
	echo "ok master"
else
	echo "trying with main"
	git push origin main
fi 
}
alias gitpush='gitpush'

function gitci {
git ci -am "$1" 
}
alias gitci='gitci'

function gitgo {
gitci "$1" && 
gitup &&
gitpush
git st
}
alias gitgo='gitgo'


