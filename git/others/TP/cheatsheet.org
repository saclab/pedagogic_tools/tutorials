* Cheatsheet ?
** Django général
*** Arborescence
```
nom_du_projet
  ├───── nom_du_projet
  │        ├───── settings.py  // 
  ├───── appli1
  │        ├───── models.py  // 
  │        ├───── urls.py  // 
  │        ├───── views.py  // 
  │





│   │   │   │       │   │       │       │   ├── controller.php.twig
│   │   │   │       │   │       │       │   ├── tests

│   │   │   │       │   │       │       │   │   └── routing.yml.twig


│   │   └── routing.yml.twig
│   │   │   │       │   │       │       │   ├─
```
*** Lancer le serveur
```./manage.py runserver```
puis aller à `localhost:8000` dans un navigateur
** Django BD
*** État courant
```./manage.py showmigrations```
*** Changer un modèle
1. Modifier `nom_de_l_application/models.py`
2. Créer les fichiers de migrations : `./manage.py makemigrations`
3. Appliquer les migrations : `./manage.py migrate`
** js
*** d3 
Un rectangle pour tout nouvel élément dans le tableau `tab`
```
d3
 .data(tab)
 .enter()
 .append("rect")
```
    
** git
*** État courant
```git status```
*** cf Thib's
** TESTS
- server side : console django (dans le terminal où on lance
  `./manage.py runserver`)
- client side : console du navigateur (Ctrl+Maj+i, Ctrl+Maj+j)
- Vider le cache du navigateur (Ctrl+Maj+r, Maj+F5)


** Clean before MR
- pas de modifications sur : manage.py, tout répertoire settings
- // bof -> supprimer migrations (`./manage.py blabla`), et refaire une mig propres
- pas d'affichage supplémentaire dans le js ou dans django
- js linter
