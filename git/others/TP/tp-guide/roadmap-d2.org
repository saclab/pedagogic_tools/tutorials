#+TITLE: TP2 : comm
#+DATE: <2019-02-25 lun.>
#+OPTIONS: author:nil ^:nil
#+LANGUAGE: fr
#+LATEX_HEADER: \usepackage[francais]{babel}
#+TAGS: solution
#+EXCLUDE_TAGS: solution
#+LATEX_HEADER: \usepackage{svg}
#+LATEX_HEADER_EXTRA:  \usepackage{mdframed}
#+LATEX_HEADER: \usepackage[margin=1in]{geometry}
# +LATEX_HEADER_EXTRA: \BeforeBeginEnvironment{minted}{\begin{mdframed}}
# +LATEX_HEADER_EXTRA: \AfterEndEnvironment{minted}{\end{mdframed}}



# +LATEX_HEADER: \usepackage{enumitem}
# +ATTR_LATEX: :environment itemize
# +ATTR_LATEX: :options [label=$\star$]


* Mise en place de l'environnement

- Au choix :
  - aller sur la page des emplois du temps de l'IUT, cliquer sur la
    breaking news de mardi, et récupérer le fichier
  - écrire au tableau :
    https://framagit.org/flopedt/FlOpEDT/blob/dev/requirements.txt (Ou
    alors http://framagit.org, puis chercher flopedt, utilisateur
    flopedt, aller sur la branche ~dev~, et récupérer ~requirements.txt~
    à la racine)
- Metttre le fichier quelque part et exécuter (ils sont ~sudo~ pour
  ~pip3~) :
#+BEGIN_SRC python
sudo pip3 install -r requirements.txt
#+END_SRC
- Aller dans le répertoire ~day-2/~ et créer le projet Django :
#+BEGIN_SRC shell
django-admin startproject minimorpion
#+END_SRC
- Regarder un peu l'arborescence.
- S'arrêter sur le [[file:../../day-2/minimorpion/minimorpion/settings.py][fichier de settings]]. On garde sqlite pour la base
  de données (flop est cependant en postgresql, on y passera jeudi)
- Et puis un truc sur lequel on leur demande de nous faire confiance :
  enlever le csrf middleware (sécurité...) dans le fichier de settings.
#+BEGIN_SRC python
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
#    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]
#+END_SRC
- Vérification de la version de python utilisée. Le point d'entrée est
  le [[../../day-2/minimorpion/manage.py][~manage.py~]]. Regarder le shebang, le mettre à ~python3~ au
  besoin. Normalement pas de problème.
- Création d'une app pour commencer
#+BEGIN_SRC shell
./manage.py startapp main_app
#+END_SRC
- Regarder un peu l'arborescence.
- Tester un coup :
#+BEGIN_SRC shell
./manage.py runserver
#+END_SRC
Faut faire des migrations visiblement
#+BEGIN_SRC shell
./manage.py migrate
#+END_SRC
- Préparer un peu la suite, structure en plus :
#+BEGIN_SRC shell
mkdir main_app/templates
mkdir main_app/templates/main_app
#+END_SRC
#+BEGIN_SRC shell
mkdir main_app/static
mkdir main_app/static/main_app
#+END_SRC
- ATTENTION : Dernier truc pour les fichiers statiques : dans le
  [[file:../../day-2/minimorpion/minimorpion/settings.py][fichier de settings]], ajouter
#+BEGIN_SRC python
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
    )
#+END_SRC


* Hello world (même si on était censé le faire dans le TP indiv)

- on veut afficher hello sur ~localhost:8000/morpion/hello~
- Plan : routeur principal envoie vers routeur de ~main_app~ qui
  envoie vers la vue :
  - on route vers main_app ce qui commence par ~morpion/~
    #+BEGIN_SRC python
    # minimorpion/urls.py
    from django.urls import path, include
    
    urlpatterns = [
        path('admin/', admin.site.urls),
        path('morpion/', include('main_app.urls'))
    ]
    #+END_SRC
  - on récupére le ~hello~ dans les urls de main_app pour les envoyer
    vers une vue
    #+BEGIN_SRC python 
    # main_app/urls.py
    from django.urls import path
    
    from main_app import views
    
    app_name="main_app"
    
    urlpatterns = [
        path('hello', views.hello, name="hello"),
    ]
    #+END_SRC
  - on crée la vue
    #+BEGIN_SRC python 
    # main_app/views.py
    from django.shortcuts import render
    
    from django.http import HttpResponse
    
    # Create your views here.
    def hello(request):
        return HttpResponse("Salut !")
    #+END_SRC  
- Exécuter 
#+BEGIN_SRC shell
./manage.py runserver
#+END_SRC
puis aller sur ~localhost:8000/morpion/hello~ dans un navigateur
(éventuellement constater que si on essaie autre chose, on a un
message de debug)



* Servir correctement les anciens fichiers

- On place les fichiers html dans ~main_app/templates/main_app~. Un
  seul fichier ici : [[file:../../day-1/morpion/final/morpion.html][~morpion.html~]] qu'on copie depuis le répertoire
  ~final~ du jour précédent.
- Pareil avec les fichiers statiques (js et css) du jour précédent
  ([[file:../../day-1/morpion/final/jquery.min.js][jquery.min.js]], [[file:../../day-1/morpion/final/d3.v4.min.js][d3.v4.min.js]], [[file:../../day-1/morpion/final/script.js][script.js]] et [[file:../../day-1/morpion/final/style.css][style.css]]), qu'on copie
  dans ~main_app/static/main_app~
- En plus des choses qu'on a fait la veille, on a ajouté un fichier
  [[file:../../day-1/morpion/final/utils.py][~utils.py~]] dans ~final~ : le copier dans le répertoire ~main_app~
- On veut servir ce qu'on a fait la veille à
  ~localhost:8000/morpion/play~ ATTENTION !! CA VA PAS MARCHER !
  - on a déjà routé ~morpion/*~ vers l'appli ~main_app~
  - les urls :
    #+BEGIN_SRC python 
    # main_app/urls.py
    urlpatterns = [
        path('hello', views.hello, name="hello"),
        path('play', views.play, name="play"),
    ]
    #+END_SRC 
  - la vue :
    #+BEGIN_SRC python
    # main_app/views.py
    def play(request):
        return render(request, 'main_app/morpion.html', {})
    #+END_SRC 
  - \rightarrow Ne trouve pas le templates.
- Ah oui, on a oublié de mettre ~main_app~ dans les appli du
  ~minimorpion/settings.py~. Maintenant, il va aller chercher s'il y a
  des templates dans ~main_app/templates~.
#+BEGIN_SRC python  
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'main_app',
]
#+END_SRC 
- Console navigateur : Aïe. 
- Il va falloir utiliser le langage de templates, dans ~morpion.html~,
  pour laisser à django le soin de gérer les adressages relatifs
  #+BEGIN_SRC html  
  <!-- main_app/templates/main_app/morpion.html -->

  <!-- vvv ICI vvv -->
  {% load static %}
  <!-- ^^^ ICI ^^^ -->
  
  <!DOCTYPE html>
  <html lang="fr">
  <head>
    <title>Morpion</title>
    <meta charset="utf-8">

    <!-- vvvvvvvvvv LA vvvvvvvvvv -->
    <link rel="stylesheet" type="text/css" href="{% static 'main_app/style.css' %}" />
    <script src="{% static 'main_app/jquery.min.js' %}"></script>
    <script src="{% static 'main_app/d3.v4.min.js' %}"></script>
    <!-- ^^^^^^^^^^ LA ^^^^^^^^^^ -->

  </head>
  <body>
   <h1>Match entre RB et FP</h1>
   <svg height="300" width="300" id="grille" >
   </svg>
   <form style="white-space: nowrap;" name="send"  action="javascript:send_game()">
     Envoyer la partie<br/></br>
     <input type="submit" name="send" value="Envoyer"></br>
   </form>
   <button type="button" onclick="new_game()">Nouvelle partie</button>
   <script>
     var player1 = "RB" ;
     var player2 = "FP" ;
    </script>

    <!-- vvvvvvvvvv ET LA vvvvvvvvvv -->
    <script type='text/javascript' src="{% static 'main_app/script.js' %}"></script>
    <!-- ^^^^^^^^^^ ET LA ^^^^^^^^^^ -->

  </body>
  </html>
  #+END_SRC
- ~localhost:8000/morpion/play~ : cool !


* Échange de données

** Client vers serveur

- On a un truc intelligent sur le serveur qui sait dire notamment qui
  a gagné. Dans un premier temps, on veut envoyer au serveur une
  partie jouée sur le client.
- On jette un coup d'œil côté client. Dans le html, on a déjà
  #+BEGIN_SRC html   
   <form style="white-space: nowrap;" name="send"  action="javascript:send_game()">
     Envoyer la partie<br/></br>
     <input type="submit" name="send" value="Envoyer"></br>
   </form>
  #+END_SRC
  On va définir ce ~send_game()~
- Il faut un truc pour réceptionner de l'autre côté.
  Dans la classe ~Game~ de ~utils.py~, on a un constructeur
  #+BEGIN_SRC python
      def __init__(self, game_json = None):
          """ Constructor that creates a default game or translates
          a json file into a Game
          """
  #+END_SRC
  qui saura transformer un json bien formé en ~Game~. On se base sur la
  doc pour créer un bon JSON côté client.
- On va réceptionner avec un argument POST. status 204, c'est :
  'cool'. Attention aux imports à rajouter.
  - les urls
    #+BEGIN_SRC python 
    # main_app/urls.py
    urlpatterns = [
        url('hello', views.hello, name="hello"),
        url('play', views.play, name="play"),
        url('save', views.save_game, name="save")
    ]
    #+END_SRC
  - la vue
    #+BEGIN_SRC python
    # main_app/views.py
    import json
    from .utils import Game
    
    # ...
    
    def save_game(request):
        game = Game(json.loads(request.POST.get('game',{})))
        game.print_game()
        
        return HttpResponse(status=204)    
    #+END_SRC
- Plus qu'à envoyer correctement depuis le côté client. Dans le ~script.js~ :
  #+BEGIN_SRC javascript
  // send the current state of the game to the server
  function send_game() {
  
      var post_args = {}
      var sent_game = {}
      sent_game['players'] = Array.from(players.keys()) ;
      sent_game['cells'] = game.cells ;
      post_args['game'] = JSON.stringify(sent_game) ;
  
      $.ajax({
          type: 'POST',
          dataType: 'json',
          data: post_args,
          url: "/morpion/save",
          success: function(msg) {
              console.log("Looks like we succeeded.")
          },
          error: function(msg) {
              console.log("Looks like we failed.")
          }
      });
  }
  #+END_SRC
- On voit que ça marche en regardant la console qui représente
  le côté serveur : console python dans le terminal
- On peut regarder différents cas.
- On peut faire mieux en ne mettant pas en dur l'adresse où aller :
  + Mettre une variable ~url_save~ dans un ~<script>~ du ~<head>~ dans
    ~morpion.html~. Attention au deux points et pas slash.
    #+BEGIN_SRC html 
    <!-- main_app/templates/main_app/morpion.html -->

    <head>
      <title>Morpion</title>
      <meta charset="utf-8">
      <link rel="stylesheet" type="text/css" href="{% static 'main_app/style.css' %}" />
      <script src="{% static 'main_app/jquery.min.js' %}"></script>
      <script src="{% static 'main_app/d3.v4.min.js' %}"></script>
      <script>
        var url_save = "{% url 'main_app:save' %}" ;
      </script>
    </head>
    #+END_SRC
  + Récupérer cette variable dans le ~script.js~
    #+BEGIN_SRC javascript 
    // main_app/static/main_app/script.js

        $.ajax({
        // ...
            url: url_save,
        // ...
        });
    #+END_SRC


** Serveur vers client \rightarrow à virer ?

- On peut essayer de le faire dans l'autre sens pour préparer
  demain. On a déjà ~export_to_json~ dans ~utils.py~ pour passer d'un
  Game à un json.
- Reste à faire l'appel ajax dans le ~script.js~ et les vues et urls.
- On a besoin de réorganiser un peu : (Réinsister sur
  l'asynchronie, vu que c'était pas clair hier.)
  + on clean un peu les déclarations de variables globales :
    #+BEGIN_SRC javascript
    var game = new Object();
    var players = new Map();
    var turns   = new Array();
    #+END_SRC
  + on commence la requête ajax 
    #+BEGIN_SRC javascript
    // get the game from an ajax request to a url
    function get_game() {
        $.ajax({
            type: "GET",
            dataType: 'text',
            contentType: "text/json",
            async: true,
            // url hardcoded for now, we will do better soon
            url: "/morpion/load",
            success: function(msg) {
    	    var parsed = JSON.parse(msg);
                console.log(parsed);
                on_game_receive(parsed);
            },
            error: function(msg) {
                console.log("Couldn't fetch the game, sorry.");
            }
        });
    }
    #+END_SRC

du coup il faut définir ~on_game_receive~, où on déporte les
anciennes initialisations (les virer de leur ancienne place)

#+BEGIN_SRC javascript
// get the game from an ajax request to a url
function on_game_receive(g) {
    game = new Object() ;
    game.cells = g.cells ;
    game.players = g.players ;
    
    // create a Map to store players informations : [name]=>{name,color,...}
    //    and an Array to represent the game turns ["n","j1","j2"]
    players = new Map();
    turns   = new Array();
    for (var i=0; i<game.players.length; i++ ) {
        players.set( game.players[i].name , game.players[i] );
        turns[i] = game.players[i].name;
    }
    turns[i] = "";
    
    display_move() ;
}
#+END_SRC

(avant, c'était :
#+BEGIN_SRC javascript 
game.players = [{"name":"FP","color":"#00FFFF"},
                {"name":"RB","color":"#FFFF00"}
               ];
game.cells = [{"x":0,"y":0, "p":"FP"},
              {"x":0,"y":1, "p":""},
              {"x":0,"y":2, "p":""},
              {"x":1,"y":0, "p":"FP"},
              {"x":1,"y":1, "p":"RB"},
              {"x":1,"y":2, "p":""},
              {"x":2,"y":0, "p":"RB"},
              {"x":2,"y":1, "p":""},
              {"x":2,"y":2, "p":"RB"}
             ];

// create a Map to store players informations : [name]=>{name,color,...}
//    and an Array to represent the game turns ["n","j1","j2"]
var players = new Map();
var turns   = new Array();
for (var i=0; i<game.players.length; i++ ) {
  players.set( game.players[i].name , game.players[i] );
  turns[i] = game.players[i].name;
}
turns[i] = "";
#+END_SRC
)
  + ajouter un appel à ~get_game()~ à la toute fin
  + faire la vue associée qui enverra la game :
    - ~main_app/urls.py~
      #+BEGIN_SRC python  
      urlpatterns = [
          path('hello', views.hello, name="hello"),
          path('play', views.play, name="play"),
          path('save', views.save_game, name="save"),
          path('load', views.load_game, name="load")
      ]
      #+END_SRC
    - ~main_app/views.py~
      #+BEGIN_SRC python  
      def load_game(request):
          game = Game()
          
          return JsonResponse(game.export_to_json(None),
                              safe=False)
      #+END_SRC
