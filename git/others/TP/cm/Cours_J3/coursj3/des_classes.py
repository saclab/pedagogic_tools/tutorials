class Person(): # class Person(object):
    def __init__(self, name, age):
        self.name = name
        self.age = age
    def is_adult(self):
        if self.age >= 18:
            return True
        else:
            return False

    def __str__(self):
        return f'{self.name} - {self.age} ans'

    def __le__(self, other):
        return self.age <= other.age
