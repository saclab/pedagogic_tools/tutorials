from django.db import models

# Create your models here.
class Person(models.Model):
    #chaque attribut définit une colonne
    name = models.CharField(null=True, max_length=30)
    age = models.IntegerField(default=18)
    best_friend = models.ForeignKey('self', on_delete=models.SET_NULL, null=True)
    #je peux définir des méthodes en plus!
    def is_adult(self):
            if self.age >= 18:
                return True
            else:
                return False
    def __str__(self):
        return f'{self.name} - {self.age} ans'

    def __le__(self, other):
        return self.age <= other.age


#Je peux définir des sous-classes
class Student(Person):
    school = models.CharField(default='IUT Blagnac', max_length=50)