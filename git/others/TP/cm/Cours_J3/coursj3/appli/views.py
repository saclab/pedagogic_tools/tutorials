from django.shortcuts import render
from appli.forms import StudentForm
from django.http import HttpResponse
from .models import Student

# Create your views here.

def StudentCreation(req):
    if req.method == 'POST':
        form = StudentForm(req.POST)
        if form.is_valid():
            data = form.cleaned_data
            return StudentRegistration(req, cleaned_data)
        else:
            return render(req, 'appli/student_creation.html', {'form': form})

    else:
        form = StudentForm()
        return render(req, 'appli/student_creation.html', {'form': form})

def StudentRegistration(req, cleaned_data):
    Student(name=cleaned_data['name']).save()
    return HttpResponse(f"L'étudiant⋅e {cleaned_data['name']} a bien été ajouté⋅e")
    
