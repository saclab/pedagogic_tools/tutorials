#+TITLE:     Semaine flop -- Jour 1 -- front end
#+AUTHOR:    FLOP
#+EMAIL:     prenaud@irit.fr
#+DATE:      [2021-03]
#+LANGUAGE: fr
#+EXCLUDE_TAGS: solution
# +LATEX_HEADER: \usepackage[frenchb]{babel}
# +LATEX_HEADER: \usepackage{svg}
# +LATEX_HEADER: \usepackage{float}
#+LATEX_HEADER: \newenvironment{tbox}{\begin{center}\begin{tabular}{|p{.08\textwidth}|}\hline\\}{\\\\\hline\end{tabular}\end{center}}
#+LATEX_HEADER: \usepackage{listings}
#+LATEX_HEADER: \usepackage{tikz}
#+LATEX_HEADER: \usepackage[T1]{fontenc}
#+LATEX_HEADER: \usepackage{lmodern}
#+LATEX_HEADER: \usepackage[utf8x]{inputenc}
#+LATEX_HEADER: \usetikzlibrary{calc}
#+LATEX_HEADER: \usetikzlibrary{positioning}
#+LATEX_HEADER: \lstset{basicstyle=\tiny}
#+LATEX_HEADER: \graphicspath{{../shared/}}
#+DESCRIPTION: Example of using org to create presentations using the beamer exporter
#+KEYWORDS:  beamer org orgmode

# specifying the beamer startup gives access to a number of
# keybindings which make configuring individual slides and components
# of slides easier.  See, for instance, C-c C-b on a frame headline.
#+STARTUP: beamer

#+STARTUP: oddeven

#+STARTUP: inlineimages

# we tell the exporter to use a specific LaTeX document class, as
# defined in org-latex-classes.  By default, this does not include a
# beamer entry so this needs to be defined in your configuration (see
# the tutorial).
#+LaTeX_CLASS: beamer
#+LaTeX_CLASS_OPTIONS: [8pt]

# Beamer supports alternate themes.  Choose your favourite here
#+BEAMER_THEME: mine

# the beamer exporter expects to be told which level of headlines
# defines the frames.  We use the first level headlines for sections
# and the second (hence H:2) for frames.
#+OPTIONS:   H:2  num:2
# toc:nil

# the following allow us to selectively choose headlines to export or not
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport

# for a column view of options and configurations for the individual
# frames
#+COLUMNS: %20ITEM %13BEAMER_env(Env) %6BEAMER_envargs(Args) %4BEAMER_col(Col) %7BEAMER_extra(Extra)



* Programmation web

** Vue d'ensemble
[[../shared/general-0.pdf]]

** Vue d'ensemble
[[../shared/general-1.pdf]]

** Vue d'ensemble
[[../shared/general-2.pdf]]

** Vue d'ensemble
[[../shared/general-3.pdf]]



* SVG
** Contexte
- Scalable Vector Graphics
- Format d’image vectoriel
- Normalisé par W3C
- Dialecte XML
- Compatible avec tous les explorateurs « modernes »

** Avantages

- Elles peuvent être zoomées à l’infini
  #+ATTR_LATEX: :height 3cm
  #+CAPTION: Source: wikipedia
  [[../shared/Bitmap_VS_SVG_Fr.pdf]]


- Elles peuvent être manipulées avec
    - Un éditeur de texte 
    - CSS
    - JavaScript


** Où ?
- Directement dans un fichier ~svg-file.svg~

  #+attr_latex: :options [t]{0.5\textwidth}
  #+begin_minipage
  #+ATTR_LATEX: :options frame=single 
  #+BEGIN_SRC xml
  <svg>
    <circle cx="0" cy="0" r="10"/>
  </svg>
  #+END_SRC
  #+end_minipage
  \hspace{.2cm}
  #+attr_latex: :options [t]{0.2\textwidth}
  #+begin_minipage
  \vfill _svg-file.svg_
  #+end_minipage

\bigskip

- puis associé à une page HTML au travers d’une balise ~<img>~

  #+attr_latex: :options [t]{0.2\textwidth}
  #+begin_minipage
  \vfill
  _~main-inc.html~_
  #+end_minipage
  \hspace{.2cm}
  #+attr_latex: :options [t]{0.6\textwidth}
  #+begin_minipage
  #+ATTR_LATEX: :options frame=single 
  #+BEGIN_SRC xml
  <html><body>
    <img src="svg-file.svg" height="64"/>
  </body></html>
  #+END_SRC
  #+end_minipage


\bigskip

- Mais la balise ~<svg>~ peut aussi être directement utilisée dans une
  page HTML

  #+attr_latex: :options [t]{0.2\textwidth}
  #+begin_minipage
  _~main-ins.html~_
  #+end_minipage
  \hspace{.2cm}
  #+attr_latex: :options [t]{0.5\textwidth}
  #+begin_minipage
  #+ATTR_LATEX: :options frame=single 
  #+BEGIN_SRC xml
  <html><body>
    <svg>
      <circle cx="0" cy="0" r="10"/>
    </svg>
  </body></html>
  #+END_SRC
  #+end_minipage


** Formes de base
Ça ressemble à quoi ?

#+BEGIN_tbox 
DEMO
#+END_tbox

\vspace{3cm}
(cf =draft.svg=)


# * Trash
# ** Apprendre ?

# ** Exemple
# ** CSS et exemple


# - Toutes les balises SVG peuvent recevoir les attributs =id= et =class=
# - les directives CSS peuvent fixer les attributs spécifiques SVG (ex : =fill=, =stroke=)
# - La plupart des directives CSS sont applicables aux éléments SVG
# - Cf. https://devdocs.io/svg-attributes/
# - Les pseudo-classes CSS sont majoritairement applicables
# - Cf.  :hover  :focus  :first-child
# - On peut également appliquer des directives d’animation
# - Cf. transform transform-origin transition



* JS

** Autour de JavaScript

- Créé par Brendan Eich en 10 jours en mai 1995
- Développé par Netscape à partir de 1995
- Standardisé dans une norme : ECMAScript
- Plusieurs implémentations de la norme :
  - JavaScript de Mozilla (version 1.8)
  - JScript de Microsoft (version 9.0)
  - ActionScript de Adobe

- Un langage « utilitaire et minimaliste » qui est devenu « fortement
  utilisé »
- Créé coté serveur (LiveScript) il a été popularisé par sa version
  client (Javascript) et il revient récemment à ses origines serveur
  (Cf. node.js ou node-webkit)



** JavaScript pour le web

- Un langage de programmation permettant de :
  - Connaître tous les composants d'une page web
  - Modifier tous les composants d'une page web
  - Piloter le navigateur qui affiche une page web

\vfill

- Code interprété par le navigateur au moment de l’exécution.
 
\vfill

- Confidentialité du code nulle (code source visible).
- Sécurité limitée (il peut être désactivé et/ou corrompu)


** HTML

- Dans les entêtes :
  - interprété au chargement de la page 
  - en principe, définition de fonctions ou de variables
  #+ATTR_LATEX: :options frame=single 
  #+BEGIN_SRC html
  <head>
  <script type="text/javascript">
  var largeurZone = 512 ;
  </script>
  </head>
  #+END_SRC

\vfill 

- Dans un fichier séparé :
  #+ATTR_LATEX: :options frame=single 
  #+BEGIN_SRC html
  <script src="mesProgs.js" type="text/javascript"></script>
  #+END_SRC

\vfill 

- Dans le corps : peut générer du contenu
  #+ATTR_LATEX: :options frame=single 
  #+BEGIN_SRC html
  <body> 
     <script type="text/javascript"> 
        document.write('<a href="url.html">lien</a>');  
     </script> 
  </body>
  #+END_SRC


** Structure - Commentaires
- Comentaires
  - ~// commenter une fin de ligne~
  - ~/* commenter une ou plusieurs lignes */~
- Séparation des instructions « ; »
  - Remplaçable implicitement par un retour chariot (en théorie)
  - Mais obligatoire pour éviter les pièges

[[../shared/semicolon-js.pdf]]

** Déclaration des variables
- ES6 introduit 2 nouveaux mots clés pour déclarer les variables : =let= & =const=
  - =var= réalise une déclaration au niveau de la fonction qui l’englobe
  - =let= et =const= réalisent des déclarations au niveau du bloc de code {...}
  - =const= crée une référence constante (la valeur est modifiable)
- Il ne faut pas utiliser les 3 notations dans les mêmes programmes on
  utilisera soit =var= soit =let= et =const=
- À terme, =var= disparaîtra probablement mais il est encore utilisé
  dans un grand nombre d’environnement de développement JS
- *Visibilité des variables* :
  - Une variable déclarée avec =var=, =let= ou =const= sera locale.
  - Une variable non déclarée sera globale
  Ce comportement est souvent source d'erreurs.

** Chaînes et tableaux
- Chaînes
#+BEGIN_SRC text
"C'est une chaîne de \"caractères\" en JavaScript"
'C\'est une chaîne de "caractères" en JavaScript'
#+END_SRC

\vfill

- Tableaux
  #+BEGIN_SRC text
  tableau = new Array(5);
  tableau = [];
  tableau = ["un", "deux", 3, false, 'cinq'];
  tableau[3] = "trois";
  #+END_SRC
  - case non initialisée ou hors borne \rightarrow ~"undefined"~
  - l'affectation d'une case hors borne entraîne son allocation.
  - longueur du tableau \rightarrow ~tableau.length~
** Structures de contrôle
Comme en Java :
- Conditionnelles
  - ~if~
  - ~switch~
- Boucles
  - ~for~
  - ~while~
  - ~do while~
** Fonctions
- Déclaration
  #+BEGIN_SRC javascript
  function prod(a,b) {
    alert("calcul du produit") ;
    var x = a * b ;
    return x ;
  }
  #+END_SRC

\vfill

- Appel
  #+BEGIN_SRC javascript
  ab = prod(5, 3) ;
  #+END_SRC

\vfill

- Rappel : visibilité des variables dans les fonctions
  - Une variable déclarée avec le mot-clé ~var~, ~let~ ou ~const~ sera
    locale.
  - Une variable non déclarée sera globale


** Paradigme objet

- =JavaScript= est basé sur la programmation orientée objet par
  prototype (programmation « sans classe » ou « à base d'instance »)
- Une propriété est une association entre un nom (aussi appelé clé) et
  une valeur
- Un objet est un ensemble de propriétés
- La valeur d'une propriété peut être une fonction, auquel cas la
  propriété peut être appelée « méthode ».
- En plus des objets natifs, il est possible de construire ses propres
  objets.
- La structure d'un objet (natif ou non) peut être modifiée à tout
  moment dans un programme =JavaScript=


# ** JSON
# inline?
# ** objet, IIFE, closure

# * Trash
# ** Plus d'instructions
# ** Opérateurs

# ** Principaux objets
# ** Constructeur

# ** Exemple
# insensible à la casse ?

# ** Documentation officielle

** DOM

- Chaque élément constitutif d'une page a un objet correspondant 

  \rightarrow DOM : Document Object Model

\vspace{1cm}

#+ATTR_LATEX: :width 0.6\textwidth
[[../shared/dom.pdf]]

\vspace{1cm}

- Depuis le js via le DOM, modification des propriétés de police de
  caractères, couleur, dimensions, découpage, filtre, visibilité,
  bordures, etc.

** Développement
- Tous les navigateurs complets actuels fournissent une console Javascript
  - Ouvrir la console / inspecter le code / outils de développement
  - Raccourcis habituels : F12, Ctrl+Shift+J, Ctrl+Shift+I, Cmd+Opt+J, ...

\vfill

- Très utile au debug

\vfill
- Affiche les erreurs à l'exécution des scripts

\vfill
- Interpréteur interactif javascript
  - inspecter l'état des variables (si visibles)
  - vérifier des propriétés
  - analyser les performances
  - (cf. ~console.log()~ / ~.assert()~, ~console.time()~ /
    ~.timeEnd()~)

\vfill

- Inspecteur d'éléments : voir le DOM

#+BEGIN_tbox 
DEMO
#+END_tbox



# * Trash
# ** Événements en js

* D3
** What
- C’est une librairie JavaScript
  - il faut inclure la librairie avant de l’utiliser
    #+BEGIN_SRC html
    <script src="d3.v4.min.js"></script>
    #+END_SRC

- D3 = Data Driven Document
  - manipule facilement le DOM
    - sélection d'élément simples
    - sélection d'éléments multiples
    - filtre d'éléments
  - manipule très bien le SVG
  - associe des données au DOM. Souvent :
    - des données récupérées du serveur stockées dans des variables ~js~
    - ... modifiées par le client
    - ... dont la représentation graphique est mise à jour
      automatiquement
    

** Why
- D3 est une librairie OpenSource
- D3 utilise une syntaxe chaînée
- les actions peuvent être enchaînée sur une même ligne «  .  »
- D3 permet de charger des données json,xml,csv ou texte
- D3 gère les transitions, les évènements, le drag and drop, etc. 

** Go

#+BEGIN_tbox 
DEMO
#+END_tbox
