#+TITLE: TP1 : Afficher des cours dans un navigateur
#+DATE: <2019-02-25 lun.>
#+OPTIONS: author:nil
#+LANGUAGE: fr
#+LATEX_HEADER: \usepackage[frenchb]{babel}
#+TAGS: solution
#+EXCLUDE_TAGS: solution
#+LATEX_HEADER: \usepackage{svg}
# +LATEX_HEADER_EXTRA:  \usepackage{mdframed}
# issue with mdframed: l3backend blabla
#+LATEX_HEADER: \usepackage[margin=1in]{geometry}
# +LATEX_HEADER_EXTRA: \BeforeBeginEnvironment{minted}{\begin{mdframed}}
# +LATEX_HEADER_EXTRA: \AfterEndEnvironment{minted}{\end{mdframed}}

#+LATEX_HEADER: \newenvironment{tbox}{\begin{center}\begin{tabular}{|p{0.9\textwidth}|}\hline\\}{\\\\\hline\end{tabular}\end{center}}

# +latex_header: \newcommand{\fraimed}[1]{\noindent\fbox{\parbox{\textwidth}{#1}}}


# +LATEX_HEADER: \usepackage{enumitem}
# +ATTR_LATEX: :environment itemize
# +ATTR_LATEX: :options [label=$\star$]


#+CAPTION: L'objectif du TP
#+NAME: fig:obj
[[./objectif.png]]



* Objectif

On souhaite afficher un fichier d'emploi du temps sous une forme
graphique, comme en Figure [[fig:obj]].


* Dessiner en programmant

#+BEGIN_tbox
On se place dans le répertoire ~day-1/flop/intro~.
#+END_tbox

  
** Fichier SVG

Sans doute avez-vous déjà rencontré des fichiers SVG (Scalable Vector
Graphics). Il s'agit d'un format d'image, comme son nom l'indique
vectorielle, sur lesquels, contrairement aux fichiers images non
vectoriels, on peut zoomer et dézoomer à volonté, sans perte de
qualité visuelle. Une image vectorielle est une assemblée d'éléments
graphiques (par exemple des segments, des rectangles, des ellipses,
etc.) placés sur un plan 2D, plutôt qu'une matrice de valeur où
chacune des cases est caractérisée par sa couleur (images bitmap).

Le format SVG est un langage de balisage basé sur XML (Extensible
Markup Language), et ressemble donc un peu au format HTML (Hypertext
Markup Language) dont vous n'ignorez pas l'existence. Sa syntaxe
suivra donc la même philosophie : on déclare une suite d'éléments. Un
élément est déclaré de la manière suivante : on ouvre la balise
(éventuellement en définissant des attributs), on déclare une suite
d'éléments, on ferme la balise. Quelque chose qui ressemble au code
du Listing [[code:xml]].

#+CAPTION: Un exemple de fichier XML
#+NAME: code:xml
#+BEGIN_SRC xml
<xml>
  <balise attribut="bidule">
    <balise_fille att1="truc" att2=12>
    </balise_fille>
  </balise>
  <balise_soeur>
    Éventuellement des choses écrites.
  </balise_soeur>
  <balise_ouvrante_fermante/>
</xml>
#+END_SRC


Les balises autorisées dans le format SVG correspondent à des éléments
graphiques, comme vous pouvez le voir sur le Listing [[code:init-svg]] qui
donne, lors de sa visualisation avec un lecteur d'image ou un
navigateur), la Figure [[fig:init-svg]].

#+CAPTION: Le code du fichier SVG
#+NAME: code:init-svg
#+INCLUDE: "./init-example.svg" src xml

#+CAPTION: L'image correspondante au fichier 
#+NAME: fig:init-svg
#+ATTR_LATEX: :width .4\textwidth
[[./init-example.svg]]


#+BEGIN_tbox
Ouvrez le fichier svg dans un éditeur de *texte*, et modifiez-le afin
d'obtenir la Figure [[fig:target-svg]].
#+END_tbox

#+CAPTION: Image voulue
#+NAME: fig:target-svg
#+ATTR_LATEX: :width .4\textwidth
[[./target-example.svg]]

*** SVG                                                            :solution:

Voir Listing [[code:target-svg]].

#+CAPTION: Code solution
#+NAME: code:target-svg
#+INCLUDE: "./target-example.svg" src xml


** Inclusion dans un fichier html

Le SVG peut être utile pour des sites webs, parce qu'il permet
d'afficher des images légères (puisque vectorielles) dans un format
qui ressemble au HTML. Des éléments SVG peuvent en effet être inclus
dans un fichier HTML et être visualisés par les navigateurs.

#+BEGIN_tbox
Créez un fichier html ~init.html~ contenant un titre et un ~body~ qui
contient votre image SVG (pas en l'important en tant qu'image, mais en
insérant le code source du SVG dans le html), et vérifiez qu'il est
bien lisible par votre navigateur préféré.
#+END_tbox


** Principe de modification par javascript 

L'idée que nous avons utilisée lors du TP collectif, et que nous
allons réuitiliser dans ce TP est la suivante : on crée un fichier
HTML, qui contient un élément ~svg~, et on insère en plus un élément
~script~ qui va accéder et modifier les éléments contenus dans le
~svg~.

#+BEGIN_tbox
- Copiez-collez [[../../day-1/morpion/intro/d3.v4.min.js][le fichier ~d3.v4.min.js~]] du répertoire
  ~day-1/morpion/intro~ vers votre répertoire courant
- Ajoutez une balise ~script~ dans votre fichier ~html~ pour inclure
  la librairie ~D3.js~.
  #+BEGIN_SRC xml
  <script src="d3.v4.min.js"></script>
  #+END_SRC
#+END_tbox



*** Premières modifications par javascript

#+BEGIN_tbox
Ajoutez une autre balise ~<script>~ juste après l'élément ~script~ où l'on
importait la librairie ~D3.js~. Placez à l'intérieur un petit code
javascript qui ajoute une forme sur l'image précédente (par exemple un
cercle). Vous aurez peut-être besoin des rappels suivants :
#+END_tbox
- ~d3.select("bal")~ renvoie le premier élément dont le nom de balise
  est ~bal~.
- ~parent.append("bali")~ ajoute un élément ~bali~ après le dernier
  élément que contient ~parent~, et renvoie cet élément.
- ~element.attr("x", 10)~ ajoute un attribut ~x~ à l'élément
  ~element~, lui assigne la valeur ~10~, et renvoie cet élément
  modifié.

**** Javascript                                                    :solution:

#+BEGIN_SRC xml
<script>
  d3.select("svg")
    .append("circle")
    .attr("cx", 20)
    .attr("cy", 20)
    .attr("r", 20)
    .attr("fill", "blue");  
</script>
#+END_SRC


* Lecture du fichier de cours

#+BEGIN_tbox
On se place dans le répertoire ~day-1/flop/core~.
#+END_tbox


Nous venons de voir comment afficher des formes dans une page HTML,
grâce à la librairie ~D3.js~, mais nous n'avons pas encore utilisé le
potentiel de cette librairie, qui lie des données à ces formes
graphiques. Continuons.

Repartons avec une structure des fichiers très similaire à la partie
précédente. Nous vous fournissons :
- un [[../../day-1/flop/core/edt.html][fichier html]] contenant :
  + une trame d'élément SVG
  + les bons imports de fichiers javascript
  + un import supplémentaire du fichier ~script.js~ 
- un [[../../day-1/flop/core/script.js][début de script]] javascript sur lequel vous allez travailler maintenant
- un [[../../day-1/flop/core/data.csv][fichier de données]] en CSV (Comma-Separated Values) qui contient
  des données d'emploi du temps
- un [[../../day-1/flop/core/style.css][fichier de style]] qui sera utile dans la partie [[sec:course-txt]]

#+BEGIN_tbox
Compléter la configuration en copiant-collant les librairies [[../../day-1/morpion/intro/jquery.min.js][~jquery~]]
et [[../../day-1/morpion/intro/d3.v4.min.js][~D3.js~]] dans le répertoire courant ~day-1/flop/core~.
#+END_tbox


** Requête ajax

La première chose à faire est de réussir à lire, depuis le
~script.js~, les données stockées dans le CSV, et les ranger dans une
structure de données dans le javascript.

Dans [[./script.js][le javascript fourni]], le fichier lu est affiché dans la console.

*** Sécurité et requêtes ajax

#+BEGIN_tbox
- Ouvrez le fichier HTML. Rien à signaler a priori. Ouvrez la console
  du navigateur (~Ctrl+Maj+i~). Zut, on ne peut pas si facilement
  faire une requête ajax sur des fichiers locaux.
- Pour contourner le problème, faisons tourner un serveur HTTP qui
  servira les fichiers contenus dans un répertoire donné. Dans un
  terminal, allez dans le répertoire contenant ~edt.html~ et lancez
  la commande
#+BEGIN_SRC shell
python3 -m http.server
#+END_SRC
- Comme nous avons nommé notre fichier html ~edt.html~, le serveur
  HTTP va servir ce fichier lorsque l'on accédera à l'adresse
  ~localhost:8000/edt.html~ depuis un navigateur par exemple. Essayez,
  constatez que ça marche, et ouvrez la console du navigateur pour
  voir si cela a bien résolu le problème d'accès aux fichiers.
#+END_tbox


** Stocker les cours

Première étape franchie, reste à interpréter correctement ce message.
Dans le fichier CSV, chaque ligne correspond à un cours placé, et
chaque colonne donne une information sur le cours placé concerné.  Le
but ici est de stocker tous les cours placés dans le javascript,
autrement dit de créer un objet par cours placé, dont les attributs
seront les colonnes du CSV. 

La fonction ~d3.csvParse~ vous sera utile. Quelques rappels :
- ~d3.csvParse~ prend deux arguments : le premier étant une chaîne de
  caractères (correspondant à ce qu'il faut lire), et le second est
  une fonction
- cette fonction prend un paramètre : un objet représentant des données
- ~d3.csvParse~ lit chaque ligne de la chaine de caractères en
  paramètre, crée un objet tel que : (1) les noms de ses attributs
  sont le nom des colonnes lues dans la chaîne de caractères, (2) les
  valeurs des attributs sont celles lues dans la ligne courante ; puis
  appelle la fonction paramètre en lui fournissant cet objet.
- fournir la fonction identité en second argument est une possibilité
  !

#+BEGIN_tbox
Modifiez ~fetch_courses~ pour stocker toutes les informations des
cours placés dans le tableau ~courses~ déjà déclaré.
#+END_tbox

Vous pouvez utiliser l'interpréteur de la console du navigateur pour
regarder si ~courses~ a bien été rempli.

*** JS parse CSV                                                   :solution:

#+BEGIN_SRC javascript 
function fetch_courses() {
    $.ajax({
        type: "GET",
        dataType: 'text',
        url: "./data.csv",
        async: true,
        contentType: "text/csv",
        success: function(msg, ts, req) {
            courses = d3.csvParse(msg, translate_courses_from_csv);
        },
        error: function(msg) {
            console.log("error");
        }
    });
}

function translate_courses_from_csv(d) {
    var co = {
        id_cours: +d.id_course,
        tutor: d.tutor_name,
        group: d.gp_name,
        module: d.module,
	c_type: d.coursetype,
        day_ref: d.day,
        start: +d.start_time,
        duration: +d.duration,
        room: d.room,
	room_type: d.room_type,
	color_bg: d.color_bg,
	color_txt: d.color_txt,
    };
    return co;
}
#+END_SRC


* Affichage 
<<sec:dis-gen>>

Familiarisons-nous avec les coordonnées en dessinant une grille qui
contiendra tous les cours que l'on souhaite afficher. Accordons-nous
sur la manière dont nous allons afficher la grille. Tout d'abord,
essayons d'afficher les cours qu'on a stockés dans ~courses~ de la
manière suivante : à chaque cours correspond un rectangle de couleur,
placé verticalement en fonction de l'heure à laquelle il aura lieu, et
horizontalement en fonction d'une part du jour où il aura lieu et
d'autre part du groupe concerné.


Voyons quelques éléments permettant d'aborder un début d'affichage.

Pour les ordonnées (position verticale des cours) :
- Dans le fichier de données ~data.csv~ :
  + les débuts de cours (~start_time~) sont donnés en minutes depuis
    0h00.
  + les durées de cours (~duration~) sont données en minutes.
- On vous propose dans le javascript la variable ~min_to_px~,
  initialisée à ~1~ : elle signifie qu'une minute dans la journée
  correspond à 1 pixel. Si un cours commence à 8h, c'est-à-dire ~8x60~
  minutes après 0h00, l'ordonnée du haut de son rectangle vaudra 400.

Pour les abscisses (position horizontale des cours) :
- À l'intérieur d'un jour :
  + Tous les groupes qui n'ont pas de sous-groupe (c'est-à-dire tous
    les groupes de TP et le groupe 4 ; appelons-les groupes de base)
    ont la même largeur. Cette largeur est stockée dans ~gp.width~
    (qui vaut ici 30, mais vous pouvez la changer).
  + La variable ~gp~, par son attribut ~nb_max~, contient le nombre de
    ces groupes de base.
- Entre les jours :
  + Dans le fichier de données, les jours sont stockés sous forme de
    chaînes de caractères (~m~ pour lundi, ~tu~ pour mardi, ~w~ pour
    mercredi, etc.)
  + Dans le javascript, dans la variable ~days~ est stocké l'ordre des
    jours.

** La grille


- Grâce aux deux premiers points sur les abscisses, calculez la
  largeur d'un jour. Créez une fonction ~day_width()~.
- Grâce au second point sur les ordonnées, calculez la hauteur d'un
  jour. Créez une fonction ~day_height()~.
- Écrivez une fonction qui prend un jour en paramètre (c'est-à-dire un
  élément du tableau ~days~) et qui renvoie l'abscisse du bord gauche
  du rectangle de ce jour. Créez une fonction ~day_x(day)~.
- Disons que l'ordonnée du bord supérieur de tous les rectangles de
  jour est 0. Créez une fonction simple ~day_y(day)~.


On vient de calculer tous les éléments qui, à partir d'un jour donné,
permettent de dessiner un rectangle correspondant à ce jour :
coordonnées du coin supérieur gauche, largeur et hauteur du rectangle.

Ne reste plus qu'à lier des rectangles aux jours. Créez une fonction
~display_grid~ qui dessine les rectangles de jours.

Quelques rappels pour effectuer cette tâche :
- ~d3.select(".name")~ renvoie le premier élément qui possède un
  attribut ~class~ qui vaut ~name~.
- ~parent.selectAll(".name").data(things).enter().append("circle").attr("cx",10)~
  dit : pour chaque nouvelle donnée dans ~things~, je crée un nouvel
  élément ~circle~, lui associe la nouvelle donnée, lui ajoute un
  attribut ~cx~ à qui je donne la valeur 10, et le place à la suite
  des éléments contenus dans ~parent~.
- L'élément svg du fichier HTML contient un groupe dont la classe est
  ~grid-layer~ ; cela paraît un bon endroit pour y placer nos
  rectangles de jour.
- Lorsqu'un élément graphique est lié à un tableau de données, et
  qu'on applique ~.attr("x", f)~ à cet élément, ~f~ peut être une
  valeur constante (par exemple 10 dans l'exemple précédent) ou une
  fonction que vous pouvez définir dans votre script. Il s'agit alors
  d'une fonction à un seul paramètre qui sera appelée par ~D3.js~ avec
  pour paramètre réel la donnée à laquelle l'élément est lié.
- Sur un rectangle ~.attr("fill", "none")~ ne remplit le rectangle
  avec aucune couleur.

# - ~var assoc = ~ associe les
#  éléments enfants de ~parent~ qui ont un attribut ~class~ valant
#  ~name~ aux données contenues dans ~things~, et stocke cette
#  association dans ~assoc~.


On ne se soucie ici que des nouvelles données, parce qu'on n'exécutera
~display_grid~ qu'une seule fois : lorsque l'on reçoit les
données. D'ailleurs, rajoutez l'appel à ~display_grid~ dans
~fetch_courses~.

*** Jours                                                          :solution:

#+BEGIN_SRC javascript
function day_width() {
    return gp.nb_max * gp.width ;
}
function day_height() {
    return (day_end - day_start) * min_to_px ;
}
function day_x(d) {
    return d.num * day_width() ;
}
function day_y() {
    return 0 ;
}
#+END_SRC


*** Display                                                        :solution:

#+BEGIN_SRC javascript
function display_grid() {
    var c_layer = d3.select(".grid-layer");

    var c_all = c_layer
        .selectAll(".day")
        .data(days) ;

    c_all
        .enter()
        .append("rect")
        .attr("class", "day")
        .attr("x", day_x)
        .attr("y", day_y)
        .attr("width", day_width)
        .attr("height", day_height)
        .attr("stroke-width", 5)
        .attr("stroke", "black")
        .attr("fill", "none");
}
#+END_SRC


*** Ajax                                                           :solution:

#+BEGIN_SRC javascript
function fetch_courses() {

    $.ajax({
    // ...
        success: function(msg, ts, req) {
            courses = d3.csvParse(msg, translate_courses_from_csv);
            display_grid() ;
        },
    // ...
    });
}
#+END_SRC


** Les cours

*** Des rectangles colorés

Le but de cette sous-partie est d'afficher les cours correctement sur
l'écran, un cours étant représenté par un rectangle dont la couleur
est la couleur du module.

Complétons ce qui a été dit en début de partie [[sec:dis-gen]] :
- Les explications sur l'ordonnée devraient suffire.
- Peut-on placer horizontalement tout élément du tableau ~courses~ à
  partir de ses attributs ? Nous pouvons trouver le jour où il aura
  lieu, et savoir donc dans quel rectangle de jour il se situera, mais
  il sera plutôt à gauche ou plutôt à droite du rectangle en fonction
  du groupe du cours.
- On vous fournit le dictionnaire ~groups~ qui associe à un nom de
  groupe des informations sur son placement horizontal. Plus
  précisément, il lui associe un objet qui a deux attributs : ~start~
  et ~width~. 
  + Chaque groupe va s'étaler sur un certain nombre de
  groupes de base : une valeur de 2 pour ~width~ indique que le groupe
  s'étale sur deux groupes de base. On rappelle que la largeur d'un
  groupe de base est stockée dans ~gp.width~.
  + Une valeur de 0 pour ~start~ indique que le groupe est aligné sur
    le bord gauche du rectangle de jour. Une valeur de 3 indique que
    le groupe est décalé de 3 groupes de base vers la droite.

Ne reste plus qu'à coder :
- Créez des fonctions ~course_x(c)~, ~course_y(c)~, ~course_width(c)~,
  ~course_height(c)~ qui prennent un élément du tableau de cours en
  paramètre et qui renvoient respectivement l'abscisse du bord gauche
  de ~c~, l'abscisse du bord supérieur de ~c~, la largeur de ~c~ et la
  hauteur de ~c~.
- Créez une fonction ~display_courses~ qui affiche les rectangles de
  couleurs correspondant aux cours en liant le tableau ~courses~ avec
  des rectangles (utilisez les fonctions du point précédent pour les
  attributs !).
- Ajoutez l'appel à ~display_courses~ dans ~fetch_courses~.

**** Coordonnées                                                   :solution:

#+BEGIN_SRC javascript
function course_x(c) {
    return idays[c.day_ref] * gp.nb_max * gp.width
        + groups[c.group].start * gp.width  ;
}
function course_width(c) {
    return groups[c.group].width * gp.width ;
}
function course_y(c) {
    return (c.start - day_start) * min_to_px ;
}
function course_height(c) {
    return c.duration * min_to_px ;
}
function course_fill(c) {
    return c.color_bg ;
}
#+END_SRC


**** Display                                                       :solution:

#+BEGIN_SRC javascript
function display_courses() {
    var c_layer = d3.select(".courses-layer");

    var c_all = c_layer
        .selectAll(".course")
        .data(courses) ;

    var c_groups = c_all
        .enter()
        .append("rect")
        .attr("x", course_x)
        .attr("y", course_y)
        .attr("width", course_width)
        .attr("height", course_height)
        .attr("fill", course_fill);

}
#+END_SRC


**** Ajax                                                          :solution:

#+BEGIN_SRC javascript
function fetch_courses() {

    $.ajax({
    // ...
        success: function(msg, ts, req) {
            courses = d3.csvParse(msg, translate_courses_from_csv);
            display_grid() ;
            display_courses() ;
        },
    // ...
    });
}
#+END_SRC


*** Informations supplémentaires
<<sec:course-txt>>

Nous y sommes presque. Nous aimerions désormais afficher des
informations textuelles sur chaque cours : le nom de son module, le
nom de l'enseignant·e, le nom de la salle.

Vous aurez besoin de méthodes non vues ce matin :
- Pour créer un élément graphique de texte "blala", il faut créer un
  élément ~text~, et lui appliquer ~.text("blabla")~.
- Étant donné le [[../../day-1/flop/core/style.css][fichier de style]] qui vous est fourni, les coordonnées
  d'un élément texte sont les coordonnées du point situé au milieu
  (horizontalement et verticalement) du texte.
- Pour grouper des éléments graphiques, on les place dans un groupe :
  ils seront les éléments enfants d'un élément ~g~.

#+BEGIN_tbox
Modifiez ~display_courses~ pour afficher les informations textuelles.
Chaque cours correspondra à un groupe qui contiendra un rectangle et
trois éléments texte.
#+END_tbox
    
**** Display                                                       :solution:

#+BEGIN_SRC javascript
function display_courses() {
    var c_layer = d3.select(".courses-layer");

    var c_all = c_layer
        .selectAll(".course")
        .data(courses) ;

    var c_groups = c_all
        .enter()
        .append("g")
        .attr("class", "course") ;

    c_groups
        .append("rect")
        .attr("x", course_x)
        .attr("y", course_y)
        .attr("width", course_width)
        .attr("height", course_height)
        .attr("fill", course_fill);

    c_groups
        .append("text")
        .text(course_module_txt)
        .attr("x",course_mid_x)
        .attr("y",course_module_y)
        .attr("fill", course_txt_fill);
    c_groups
        .append("text")
        .text(course_tutor_txt)
        .attr("x",course_mid_x)
        .attr("y",course_tutor_y)
        .attr("fill", course_txt_fill);
    c_groups
        .append("text")
        .text(course_room_txt)
        .attr("x",course_mid_x)
        .attr("y",course_room_y)
        .attr("fill", course_txt_fill);
    
}
#+END_SRC
