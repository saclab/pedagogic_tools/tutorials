#+TITLE: TP3 : Python, modèles
#+DATE: <2021-03-03 mer.>
#+OPTIONS: author:nil
#+LANGUAGE: fr
#+LATEX_HEADER: \usepackage[francais]{babel}
#+TAGS: solution
#+EXCLUDE_TAGS: solution
#+LATEX_HEADER: \usepackage{svg}
# +LATEX_HEADER_EXTRA:  \usepackage{mdframed}
#+LATEX_HEADER: \usepackage[margin=1in]{geometry}
# +LATEX_HEADER_EXTRA: \BeforeBeginEnvironment{minted}{\begin{mdframed}}
# +LATEX_HEADER_EXTRA: \AfterEndEnvironment{minted}{\end{mdframed}}

#+LATEX_HEADER: \newenvironment{tbox}{\begin{center}\begin{tabular}{|p{0.9\textwidth}|}\hline\\}{\\\\\hline\end{tabular}\end{center}}


# +LATEX_HEADER: \usepackage{enumitem}
# +ATTR_LATEX: :environment itemize
# +ATTR_LATEX: :options [label=$\star$]

* Les modèles

** Spécifications

Voici une description des modèles utilisés dans ~base~.

#+OPTIONS: ^:nil
|-----------------+---------------+-------------------------------------------------------|
| Classes         | Attributs     | Propriétés/Description                                |
|-----------------+---------------+-------------------------------------------------------|
| Group           | name          | son nom                                               |
|                 | parent_group  | son surgroupe direct                                  |
|-----------------+---------------+-------------------------------------------------------|
| RoomType        | name          | les types de salle (TD, TP, Amphi...)                 |
|                 |               |                                                       |
| Room            | name          | les salles, qui chacune peut avoir plusieurs          |
|                 | room_type     | types (la B007 est une salle de TD et de TP)          |
|-----------------+---------------+-------------------------------------------------------|
| Module          | name          | Son nom                                               |
|                 | abbrev        | Son nom abrégé (qui doit être limité à 10 caractères) |
|                 | head          | Le responsable de modules                             |
|                 |               |                                                       |
| Course          | group         |                                                       |
|                 | tutor         |                                                       |
|                 | module        |                                                       |
|                 | room_type     |                                                       |
|                 | week          | Un nombre entre 0 et 53                               |
|                 | year          |                                                       |
|                 | duration      | La durée du cours, en minutes                         |
|                 |               |                                                       |
| ScheduledCourse | course        | Le cours à placer...                                  |
|                 | day           | models.CharField(max_length=2, default=Day.MONDAY,    |
|                 |               | choices=[(d, d.value) for d in Day])                  |
|                 | start_time    | En minutes à partir de minuit                         |
|                 | room          |                                                       |
|-----------------+---------------+-------------------------------------------------------|
| ModuleDisplay   | module        | C'est du OneToOne!                                    |
|                 | color_bg      |                                                       |
|                 | color_txt     |                                                       |
|                 |               |                                                       |
| GroupDisplay    | group         |                                                       |
|                 | button_height |                                                       |
|                 | button_txt    |                                                       |
|                 |               |                                                       |
|-----------------+---------------+-------------------------------------------------------|

Voici, pour rappel, comment on a défini la classe ~Group~ dans le fichier
~base/models.py~:

#+BEGIN_SRC python
class Group(models.Model):
    name = models.CharField(max_length=4)
    parent_group = models.ForeignKey('self',
                                     blank=True,
                                     null=True,
                                     related_name="children_group",
                                     on_delete=models.CASCADE)
                                     
    #On peut surcharger la fonction __str__ afin d'avoir un affichage plus 
    #joli en cas de print
    def __str__(self):
        return self.name
#+END_SRC

#+BEGIN_tbox
- Rémplacer le fichier ~base/models.py~ par le fichier ~models_with_bugs.py~ (que vous renommerez ~models.py~).
- Définir la classe ~Course~, qui correspond aux blocs de cours à placer.
- Exécuter un ~runserver~. Lire les erreurs et corriger le ~base/models.py~ en fonction. 
  Après vos corrections, s'il ne se plaint pas, félicitations ! 
Voici les classes de ~Model.models~ qui pourraient vous être utiles:
~CharField~, ~ForeignKey~, ~ManyToManyField~, 
~PositiveSmallIntegerField~, ~OneToOneField~, 
#+END_tbox

ATTENTION : Les autres classes sont définies, mais des erreurs se sont glissées, qui seront 
à corriger au fur et à mesure du TP...

Notez qu'on définit également la classe ~Day~ qui hérite de la classe ~Enum~ 
(pour pouvoir faire référence aux jours de la semaine):

#+BEGIN_SRC python
class Day(Enum):
    MONDAY = "m"
    TUESDAY = "tu"
    WEDNESDAY = "w"
    THURSDAY = "th"
    FRIDAY = "f"
    SATURDAY = "sa"
    SUNDAY = "su"
#+END_SRC

** Ressources

#+BEGIN_tbox
Décommenter les lignes commentées ~base/admin.py~. 
Exécuter un ~runserver~.
Lire les erreurs et corriger le ~base/models.py~ en fonction.
Après vos corrections, s'il ne se plaint pas, félicitations ! 
#+END_tbox

Lorsqu'il n'y a plus d'erreur, cela ne veut pas forcément dire que le
~models.py~ est bon. Mais en tout cas, on n'a pas de preuve évidente qu'il
ne l'est pas.

** Les modèles 							   :solution:
#+BEGIN_SRC python

from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from enum import Enum

[...]

class RoomType(models.Model):

class Room(models.Model):
    room_type = models.ManyToManyField(RoomType)
    # S'il rajoute le on_delete, devra être corrigé après (deploy_base)

# </editor-fold>

# <editor-fold desc="COURSES">
# -------------
# -- COURSES --
# -------------


class Module(models.Model):
    head = models.ForeignKey('people.Tutor',
                             ...)


class Course(models.Model):
    room_type = models.ForeignKey('RoomType', null=True, on_delete=models.CASCADE)
    tutor = models.ForeignKey('people.Tutor',
                              related_name='taught_courses',
                              null=True,
                              default=None,
                              on_delete=models.CASCADE)

    group = models.ForeignKey('Group', on_delete=models.CASCADE)
    module = models.ForeignKey('Module', related_name='module', on_delete=models.CASCADE)
    duration = models.PositiveSmallIntegerField(default=90)
    week = models.PositiveSmallIntegerField(
        validators=[MinValueValidator(0), MaxValueValidator(53)],
        null=True, blank=True)
    year = models.PositiveSmallIntegerField()

    def __str__(self):
        return f"{self.module}-{self.tutor.username}-{self.group}"


class ScheduledCourse(models.Model):
    course = models.ForeignKey('Course', on_delete=models.CASCADE)
    [...]
    room = models.ForeignKey('Room', blank=True, null=True, on_delete=models.CASCADE)


# </editor-fold desc="COURSES">


# <editor-fold desc="DISPLAY">
# -------------
# -- DISPLAY --
# -------------


class ModuleDisplay(models.Model):
    module = models.OneToOneField('Module', related_name='display',
                                  on_delete=models.CASCADE)


# </editor-fold desc="DISPLAY">
#+END_SRC


* Remplir la base 

** Migrations 

Le ~runserver~ fait des vérifications au niveau des classes python :
il vérifie que les classes existent bien dans ~models.py~ et qu'elles
comportent bien les bons attributs. Il ne vérifie pas l'adéquation des
modèles Python avec la base de données qui est censée stocker ces
objets.

Tant que l'on ne dit pas explicitement au gestionnaire de BD de faire évoluer
la structure de la base de données, celle-ci ne prend pas en compte
les changements de ~models.py~.

#+BEGIN_tbox
Faire évoluer la base en construisant les fichiers de migration, puis
en appliquant les migrations.
#+END_tbox

*** Django mig 							   :solution:

#+BEGIN_SRC shell
./manage.py makemigrations
./manage.py migrate
#+END_SRC


** Stockage des données

Django vient avec une console interactive, nommée ~shell~, qui vous donne accès
 à l'environnement du framework. Vous pourrez en particulier importer les modèles pour
définir et manipuler des objets, utiliser les méthodes et jouer avec...
#+BEGIN_tbox
- Lancer un shell django ~./manage.py shell~.

- Exécuter le script de remplissage de la base de données ~import deploy_base~.

- Là encore, il va se plaindre. Lire les erreurs et corriger le ~base/models.py~ en fonction. 
#+END_tbox
Attention, à chaque modification il vous faudra éventuellement créer des migrations et les appliquer!

*** deploy_base 						   :solution:

#+BEGIN_SRC python
class Room(models.Model):
    name = models.CharField(max_length=20)
    room_type = models.ManyToManyField(RoomType,...)

class Module(models.Model):
    name = models.CharField(max_length=100, null=True)
    [...]

class GroupDisplay(models.Model):
    [...]
    button_txt = models.CharField(max_length=20, null=True, default=None)
#+END_SRC

** Quelques tests
Une fois corrigé, dans le shell django vous pouvez essayer quelques requêtes pour vérifier 
que les données ont bien été stockées. Pour cela, vous pouvez lancer un ~shell~ Django et 
importer les modèles:

#+BEGIN_SRC python
from base.models import Course, Group
from people.models import *
#+END_SRC

#+BEGIN_tbox

- Afficher dans la console tous les cours du professeur ~PSE~.

- Afficher tous les cours placés du module ~FLOP~.

- Ajouter un ~Tutor~ en base, avec le username ~patate~ et le mot de passe ~passe~.

Plus dur:
- Afficher dans la console tous les cours placés qui commencent à 8h le lundi 
- Afficher ceux qui commencent à 8h le lundi ou à 11h le mardi.
#+END_tbox

*** Tests ORM 							   :solution:
#+BEGIN_SRC python
from base.models import Course, ScheduledCourse
from people.models import Tutor

Course.objects.filter(tutor__username='PSE')
ScheduledCourse.objects.filter(course__module__abbrev='FLOP')
t=Tutor(username='patate')
t.save()
t.set_password("passe")

#+END_SRC
* Servir un fichier contenant les infos des cours

Le fichier ~base/admin.py~ nous a notamment servi à vérifier
l'adéquation du ~base/models.py~ avec ses spécifications. Il va
désormais surtout être utile pour faire passer les informations de
cours depuis la base de données vers le javascript.

Le javascript a besoin d'un fichier CSV ; jusqu'à présent, nous
utilisions les fichiers statiques ~base/static/base/dataXXXX.csv~.
Ces fichiers ont été créés une fois pour toute, et ne permettent pas
de refléter l'état d'une base de données qui peut évoluer.

#+BEGIN_tbox
Le template ~base/edt.html~, la vue ~display~ de ~base/views.py~ et l'url ~display~
de ~base/urls.py~ ont été modifiés de telle sorte qu'ils prennent en argument 
une semaine en plus d'une année.
- Modifier la vue ~fetch_courses~ dans ~base/views.py~,  de telle sorte qu'elle utilise 
  ~ScheduledCourseResource~ fournie dans ~base/admin.py~ pour créer des fichiers CSV à la volée
  étant données une semaine et une année (au lieu de renvoyer un CSV statique comme avant...)
#+END_tbox

** Vue fetch							   :solution:
Dans ~base/views.py~:
#+BEGIN_SRC python
from base.admin import ScheduledCourseResource
from base.models import ScheduledCourse

def fetch_courses(req, year=None, week=None):
    # if year is None or week is None:
    #     year = 2020
    #     week = 10
    try:
        year = int(year)
        week = int(week)
    except ValueError:
        return HttpResponse("KO")
        
    dataset = ScheduledCourseResource() \
        .export(ScheduledCourse.objects.filter(
                    course__week=week,
                    course__year=year)
        )

    response = HttpResponse(dataset.csv, content_type='text/csv')
    
    return response
#+END_SRC

Dans ~edt.html~:
#+BEGIN_SRC html
var year = {{ year }} ;
var week = {{ week }} ;
var url_data = "{% url 'base:fetch_scheduled_courses' year=year week=week %}" ;
#+END_SRC

Dans ~base/urls.py~:
#+BEGIN_SRC python
    url(r'display/((?P<year>\d{4}))?(/(?P<week>\d{1,2}))?', views.edt, name="edt"),
    url(r'fetch_scheduled_courses/(?P<year>\d+)/(?P<week>\d+)/',
        views.fetch_scheduled_courses,
        name="fetch_scheduled_courses"),
#+END_SRC
* Mise en application 
** de la base à l'affichage
En s'inspirant de ce qu'on a vu, on va créer une page qui affiche la liste des modules
avec leur abbreviation, leur nom, et l'accronyme de leur responsable (en les triant par ordre alphabétique de responsable)

Pour cela:
#+BEGIN_tbox
- créer un objet de la classe ~resources.ModelResource~ dans ~base/admin.py~ en vue de
  créer un .csv contenant ce qui vous intéresse.

- dans ~base/views.py~, créer une fonction qui
  renvoie un fichier ~dataset.csv~ contenant les informations voulues
  (à l'aide de la fonction ~HttpResponse~)

- ajouter un chemin dans ~base/urls.py~, permettant
  d'appeler la fonction ci-dessus.

#+END_tbox

L'accès à l'url choisie vous renverra donc un csv contenant les
informations pertinentes.
Reste alors à l'afficher, pour cela :
#+BEGIN_tbox
- créer un template dans ~base/templates/base~ qui utilisera le
  fichier ~dataset.csv~ comme une variable et l'affichera ~joliment~
  (à vous de voir à quel point!).
- créer une fonction dans ~base/views.py~ qui renvoie ce template.
- créer un chemin dans ~base/urls.py~ qui appelle cette fonction.
#+END_tbox
Ne reste plus qu'à tester!

** Formulaire d'ajout d'un module
On va s'entraîner aux ModelForms de Django en fabriquant une page html contenant un formulaire ad hoc. Pour cela:
#+BEGIN_tbox
- créer un formulaire ~GroupForm~ de la classe ~forms.ModelForm~ dans ~base/forms.py~ 

- dans ~base/templates/~ ajouter un template pouvant accueillir ce formulaire

- dans ~base/views.py~, créer une fonction ~add_group~ qui
  renvoie ce template avec un nouveau formulaire si la requête est ~GET~, et si la requête est ~POST~ un formulaire rempli si les données ne sont pas valides,
  et une vue d'enregistrement si elles le sont.

- dans ~base/views.py~ toujours, ajouter une fonction ~register_group~ qui prend en entrée une requête et des données valides, 
  qui ajoute un objet à la base de données (avec la méthode ~.save()~ et qui renvoie une page de validation (à l'aide de la fonction ~HttpResponse~).

- ajouter un chemin dans ~base/urls.py~, permettant
  d'appeler la fonction ~add_group~ ci-dessus.

#+END_tbox
* Création de méthodes Python utiles
** Redéfinition de la méthode d'impression
Regarder ce que renvoie la méthode ~print~ pour les objets que vous avez créés.
#+BEGIN_tbox
Modifiez la méthode ~__str__~ pour que ça imprime quelque chose de lisible dans la console.
#+END_tbox

** Arbre des groupes
#+BEGIN_tbox
En vous inspirant de la méthode ~ancestor
_groups~ de ~Group~ (qui renvoie l'ensemble des groupes 
qui contiennent le groupe en question) ajouter une méthode ~decendant_groups~ qui
renvoie l'ensemble de tous les sous-groupes du groupe en question.
#+END_tbox

** Plein d'autres
Que vous définirez...

* Explorer le code de flop!EDT
S'il vous reste du temps, vous pouvez cloner le dépôt du logiciel
#+BEGIN_SRC shell
https://framagit.org/flopedt/FlOpEDT.git
#+END_SRC
et explorer la branche ~dev~... En particulier, vous pouvez commencer à identifier 
les fichiers qu'il va vous falloir modifier pour votre projet
