/*------------------------
  ------ VARIABLES -------
  ------------------------*/

// input
var days = [{num: 0, ref: "m", name: "Lun."},
            {num: 1, ref: "tu", name: "Mar."},
            {num: 2, ref: "w", name: "Mer."},
            {num: 3, ref: "th", name: "Jeu."},
            {num: 4, ref: "f", name: "Ven."}] ;
var idays = {};
var groups = {} ;
var day_start = 0 ;      //8*60 ;
var day_end = 24*60 ;    //18*60 + 45 ;


// data
var courses = [] ;

// display settings
var min_to_px = 1 ;
var gp = {width: 30, nb_max: 7} ;


// initialisation function
function init() {
    
    d3.select("svg")
        .attr("height", svg_height())
        .attr("width", svg_width()) ;

    for(var i = 0 ; i<days.length ; i++){
        idays[days[i].ref] = days[i].num ;
    }

    groups["CE"] = {start: 0, width: 6} ;
    groups["1"] =  {start: 0, width: 2} ;
    groups["1A"] = {start: 0, width: 1} ;
    groups["1B"] = {start: 1, width: 1} ;
    groups["2"] =  {start: 2, width: 2} ;
    groups["2A"] = {start: 2, width: 1} ;
    groups["2B"] = {start: 3, width: 1} ;
    groups["3"] =  {start: 4, width: 2} ;
    groups["3A"] = {start: 4, width: 1} ;
    groups["3B"] = {start: 5, width: 1} ;
    groups["4"] =  {start: 6, width: 1} ;
    groups["234"]= {start: 2, width: 5} ;
}



/*------------------------
  ---- READ DATA FILE ----
  ------------------------*/

function fetch_courses() {

    $.ajax({
        type: "GET",
        dataType: 'text',
        url: "./data.csv",
        async: true,
        contentType: "text/csv",
        success: function(msg, ts, req) {
            tutors = [];
            modules = [];
            rooms = [];
            
            courses = d3.csvParse(msg, translate_courses_from_csv);
            
            //BEGIN Team Alpha code

            //END Team Alpha code
            			
// **********************************************			
//            display_courses() ;
//            display_grid() ;
//            display_tutor_filters() ; 
// **********************************************
 
 //displayDropDown();
 
            //BEGIN Team Bravo code

            //END Team Bravo code         
        },
        error: function(msg) {
            console.log("error");
        }
    });

}


//BEGIN Team Alpha code

//END Team Alpha code


//BEGIN Team Bravo code

//END Team Bravo code  


function translate_courses_from_csv(d) {
    var ind = tutors.indexOf(d.prof_nom);
    if (ind == -1) {
        tutors.push(d.prof_nom);
    }
    if (modules.indexOf(d.module) == -1) {
        modules.push(d.module);
    }
    if (rooms.indexOf(d.room) == -1) {
        rooms.push(d.room);
    }
    var co = {
        id_cours: +d.id_course,
        tutor: d.tutor_name,
        group: d.gp_name,
        module: d.module,
	c_type: d.coursetype,
        day_ref: d.day,
        start: +d.start_time,
        duration: +d.duration,
        room: d.room,
	room_type: d.room_type,
	color_bg: d.color_bg,
	color_txt: d.color_txt,
    };
    return co;
}




function svg_height() {
    return (day_end - day_start) * min_to_px + 200 ;
}
function svg_width() {
    return days.length * gp.nb_max * gp.width ;
}
/*------------------
  ------ RUN -------
  ------------------*/

init();
fetch_courses() ;
