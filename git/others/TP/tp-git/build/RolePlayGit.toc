\babel@toc {english}{}
\contentsline {section}{\numberline {1}Preparation de l'espace de travail}{1}{section.1}
\contentsline {subsection}{\numberline {1.1}Introduction}{1}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Cr\IeC {\'e}ation d'un compte framagit}{2}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Constitution des bin\IeC {\^o}mes}{3}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}R\IeC {\'e}cuperation du depot distant: git clone}{4}{subsection.1.4}
\contentsline {subsection}{\numberline {1.5}R\IeC {\'e}cuperation du projet de page web "initialWebPage"}{4}{subsection.1.5}
\contentsline {subsection}{\numberline {1.6}Premier commit}{5}{subsection.1.6}
\contentsline {subsection}{\numberline {1.7}Pr\IeC {\^e}ts \IeC {\`a} travailler en \IeC {\'e}quipe !}{5}{subsection.1.7}
\contentsline {section}{\numberline {2}Premiers pas chacun de votre c\IeC {\^o}t\IeC {\'e} : Partie Alpha Team}{6}{section.2}
\contentsline {subsection}{\numberline {2.1}Pr\IeC {\'e}parez (r\IeC {\'e}cuperez) votre espace de travail}{6}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Exercice 1 - Afficher plus d'informations sur la page HTML -\\ Alpha Team}{6}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}Travail individuel}{6}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2}Synchronisation avec votre bin\IeC {\^o}me}{7}{subsubsection.2.2.2}
\contentsline {subsection}{\numberline {2.3}Exercice 2 - Premiers conflits potentiels}{7}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Exercice 3 : Travail d'\IeC {\'e}quipe - temporiser}{8}{subsection.2.4}
