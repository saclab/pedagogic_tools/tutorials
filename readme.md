# Welcome to your lab tools checklists !

This set of steps and tools are highly recommended for working with a consistent set of tools and methods, same as we are use to in the lab...

# ISAE tools check-list

1 - Create gitlab (isae) account 

[there](https://gitlab.isae-supaero.fr/users/sign_in)

(Click on DESK Ticket.)
```
Subject: Gitlab Account
Bonjour,

Pourriez-vous s'il-vous-plait me permettre d'accéder à gitlab.isae.fr

Bien cordialement,
```

2 - tell me when it's done

3 - Check Access to your account (provided by your tutor)

4 - Rocket Chat account [there](https://rocketchat.isae.fr) - send me a 'hello'

5 - tutorials : 
 *  git tutorial
             - different sort [here](https://gitlab.isae-supaero.fr/saclab/pedagogic_tools/tutorials/-/tree/master/git)
             - (e.g [this one](https://www.youtube.com/watch?v=USjZcfj8yxE))
             - Windows tutorial [there](https://gitlab.isae-supaero.fr/saclab/pedagogic_tools/tutorials/-/raw/master/git/gitlab_tutorial_for_students.pdf?inline=false)
 * markdown (the readme.md of gitlab/github...) [here](https://www.markdownguide.org/cheat-sheet)          
 

6 - your gitlab project

 - write a reamde.md, [MarkDown format](https://www.markdownguide.org/basic-syntax/)  containing at least : 
 
       * the structure of your repo
       * a quickstart guide (to lauch our work)
       * dependencies (including conda env management)
       * thethe full install process of your developments.

 - a src/ folder will contain your code
 - a doc/ folder will contain your report, biblio etc. (and include the papers you have found) 
 - good practices : 

        * do not push binary files (exception on .pdf for your report to open it fast, images & cie for easy compilation). For large binary files (e.g. viedos) see below
        * use english
        * at least 1 commit + push a day. But as many as you want. Use explicit messages with a "tag" in brackets for your commit messages (e.g: "[dev] implementing awesome feature showing that P = NP ")
        * use latex for your reports (you can use overleaf, but be shure to push src + pdf on your gitlab too)
        * put large binaries files on the gitlab package. Tutorial at the end of this readme.

7 - if you are using python, use conda envrionment
e.g. : 
```
conda create -n my_env -c anaconda python=3.9.7
conda activate my_env
conda install pip
pip install --upgrade python-gitlab-3.4.0 argparse-1.4.0 progress-1.6 openyxl-3.0.9 pandas-1.4.2
pip install spyder‑kernels==2.1
```
when going back on your work, be sure to activate your env ! 

```
conda activate my_env
```


8 - images and report
use vectorial imaging (pdf) and provide sources (dot, svg, odt, ppt...) 


9 - for your report, use latex, 

ex: [overleaf](overleaf.com/learn/latex/Bibliography_management_with_bibtex )

10 - by default, all in english

# package registry, putting heavy files on gitlab: 

## EN

Traditionally, we avoid putting too big files on the git, especially when it's bianire (not ASCII)

exception for .doc / .ppt / odt / pdf... because it's still practical to manipulate in the git and it evolves a lot...


To add a file in package, we'll use curl. First, a token is needed.

 - gitlab -> the project -> settings -> access token

 - choose all scopes

- get a token (as a key) and keep it safe somewhere.

- to add a binary file: 

curl --header "PRIVATE-TOKEN: myToken" --upload-file openmpmpibench_C.tar.gz "https://gitlab.isae-supaero.fr/api/v4/projects/myid/packages/generic/my_package/0.0.1/myname?status=default"

by replacing: myToken => replace with :

your string 'myid' = the id of your project, you can get it when you connect to gitlab click on your project below the project name you'll see "project id" it's a number

'my_package' = the name of the package you want to create (it can be the same as the file name or different it's up to you) 0.0.1 => the version of your package to change if you want

'myname' => the name of the file you want

exemple (just for the  syntaxe): curl --header "PRIVATE-TOKEN: SW-x164HKBkUHyDyFshC" --upload-file JSatOrb_StandAlone_v01.tar.bz2 "https://gitlab.isae-supaero.fr/api/v4/projects/163/packages/generic/JSatOrb_StandAlone/1.0.0/JSatOrb_StandAlone?status=default"

it gives things like that (video of Gérald) :

https://gitlab.isae-supaero.fr/hy7one/presentations/hycube/-/packages/24

## FR 

Traditionally, we avoid putting too big files on the git, especially when it's bianire (not ASCII)

exception for .doc / .ppt / odt / pdf... because it's still practical to manipulate in the git and it evolves a lot...

Guillaume Garrouste nous a fait un tuto pour les placer dans le registry :

Pour ajouter un fichier a un package il faut utiliser curl
Mais d'abord il faut créer un token
Donc : settings -> access token

Tu donnes un nom tu coches tout et tu crées
En retour il va te donner un jeton (a ne pas perdre) sous la forme d'une clé
Ensuite pour ajouter par exemple un fichier binaire tu feras :

curl --header "PRIVATE-TOKEN: myToken" --upload-file openmpmpibench_C.tar.gz "https://gitlab.isae-supaero.fr/api/v4/projects/myid/packages/generic/my_package/0.0.1/myname?status=default"

en remplacant:
myToken => remplacer par votre chaine de caractère
myid = l'id de son projet, on peut la récupérer lorsque on se connecte à gitlab cliquer sur votre projet en dessous du nom du projet vous verrez "identifiant du projet" c'est un numéro
my_package = le nom du package qu'on veut créer (ça peut être le même que le nom du fichier ou différent c'est à vous de voir)
0.0.1 => La version de son package à changer si vous voulez
myname => le nom du fichier qu'on veut

exemple (un exemple bidon vite fait pas cohérent, c'est juste pour la syntaxe):
curl --header "PRIVATE-TOKEN: SW-x164HKBkUHyDyFshC" --upload-file JSatOrb_StandAlone_v01.tar.bz2 "https://gitlab.isae-supaero.fr/api/v4/projects/163/packages/generic/JSatOrb_StandAlone/1.0.0/JSatOrb_StandAlone?status=default"

it gives things like that (video of Gérald) :

https://gitlab.isae-supaero.fr/hy7one/presentations/hycube/-/packages/24

## package registry 

```bash
TOKEN=XXXXXXXXXX
FILE=JSatOrb_StandAlone_v01.tar.bz2

URL=https://gitlab.isae-supaero.fr/api/v4/projects
ID=163
PATH=packages/generic/JSatOrb_StandAlone/1.0.0/JSatOrb_StandAlone?status=default

curl --header "PRIVATE-TOKEN: ${TOKEN}" --upload-file ${FILE} "${URL}/${ID}/${PATH}"
```

