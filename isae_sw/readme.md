
# Softwares 2024-2025

## Install sous WINDOWS :

### Outils internes ou installeurs "classiques" : 

 - Software - Satorb - v7.0 - [download](https://sourceforge.isae.fr/projects/miniproject_intro_space_systems/repository/revisions/master/raw/supportLight/Satorb_V07_setup.exe) : 
   - Installation classique via setup windows.
   - idéalement dans D:/CAS/Satorb_V07

---
 - Simusat v6.4.3 - Standalone - [download](https://gitlab.isae-supaero.fr/gt-dcas/simusat-group/simusat-dev/-/package_files/37/download) : 
    - placer le zip sous D:/ et le décompresser
    - idéalement dans D:/CAS/Simusat_v6.4.3 
    - Rq : pas d'installation requise, mais vérifier qu'il se lance sur les comptes étudiants
---
 - Software - Stela - v3.1.4 - Disponible en lisgne, sur le site du CNES.
 sinon, une version de secours v3.1.1 est disponible:  [download](https://sourceforge.isae.fr/projects/miniproject_intro_space_systems/repository/revisions/master/raw/supportLight/stela-install-3.1.1.jar)
    - disposer d'une JVM sous windows
    - Rq : pas d'installation requise, mais vérifier qu'il se lance sur les comptes étudiants     
---
 - IDM-CIC et IDM view
    - pas d'install, mais les logiciels doivent être accessibles aux étudiants, ils sont actuellement sur les disques [Samba](smb://partage-etu/idm-cic/)
    - A chaque cours les étudiants doivent l'installer sur leur session, en suivant le mode d'emploi fourni.



### SYSTEMA Thermica

Besoin du logiciel Airbus Défence / licences SYSTEMA THERMICA - licence Airbus gratuite -F5575

Il faut renouveller la licence chaque année auprès des contact Airbus - [Cf Desk](https://desk.isae.fr/issues/222454)

Information complémentaires sur la Forge, avec les fichiers appropriés : [ici](https://forge.isae.fr/issues/5575)

### Sous Icare/windows (ou sur l'ensemble des poste windows)

 - MSproject (normalement, il est toujours sur la machine DCAS-CATIA)


## Install sous LINUX (Ubuntu) :

### Pour le parcours SSA :
 - liste des modules requis
   - python (module load anaconda3/2023)
   - julia (module load julia/1.9.3)
   - ruby (natif sous ubuntu)
   - HyperTensioN (script Ruby)
   - FF (binaire C++)
 - dépendances
   - graphviz (sudo apt install graphviz)
   - evince (sudo apt install evince)

   - python [requirments.txt](https://gitlab.isae-supaero.fr/saclab/ssa/dependencies/-/raw/main/requirements.txt?ref_type=heads&inline=false)
   - Julia [module](https://gitlab.isae-supaero.fr/saclab/ssa/dependencies/-/raw/main/julia?ref_type=heads&inline=false)
   - FF binary [build](https://gitlab.isae-supaero.fr/saclab/ssa/dependencies/-/tree/main?ref_type=heads#ff-c)
   - script pour charger l'environnement [loadEnv.bash](https://gitlab.isae-supaero.fr/saclab/ssa/intro-htn/-/raw/main/loadEnv.bash?ref_type=heads&inline=false)

 - Page pour la procédure d'install exhaustive sur les machiens du SI: [ici](https://gitlab.isae-supaero.fr/saclab/ssa/dependencies#in-si-machines)

### Install sous Icare/Linux : 

 - JSatOrb_v1.0.b - [JSatOrb_v1.0.b.tar.bz2](https://gitlab.isae-supaero.fr/nanostar/jsatorb/-/packages/45)

Un readme est disponible dans l'archive, et un guide d'isntallation est disponible [ici](https://gitlab.isae-supaero.fr/nanostar/jsatorb).

 - dépendances : 
   - docker
   - VTS
   - firefox
  
 - Machine ICARE déployées: dcas-jsatorb-tpl-RDP, dcas-jsatorb1-RDP, ..., dcas-jsatorb8-RDP,




More guides on [SacLab repo](https://gitlab.isae-supaero.fr/saclab/pedagogic_tools/tutorials)!





# Depracated

 - Simusat VB6 - [download](https://sourceforge.isae.fr/projects/miniproject_intro_space_systems/repository/revisions/master/raw/softs/oldVersions/Simusat_Vb6_V03_setup.exe) : 
    - Installation classique via setup windows.
    - idéalement dans D:/CAS/Simusat_OldVersion
    - Rq : installation de backup suivant la version que les intervenants souhaitent utiliser
---
